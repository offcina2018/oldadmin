/**
 * Created by paulomarinho on 28/05/17.
 */
export class AppSettings {
    public static API_ENDPOINT = 'http://liame.org.br:3000/v1';
    public static apitoken = 'NOMORE';
    public usuarioLogado: any;

}
export enum statusAssociado {
    pendente = 0,
    ativo = 1,
    inativo = 2,
    negado = 3,
    desligado = 4,
    excluido = 5
}
export enum statusEmpresa {
    pendente = 0,
    cadastrada = 1,
    excluido = 2
}
export enum statusNotaFiscal {
    /*
    Solicitada (pedido recebido)
Negada (não pode ser emitida)*
Comentada (comentário feito )*
Gerada (enviada e aguardando pagamento)*
Cancelada*
Substituída (taxa R$10,00)*
Pendente (NF emitida + que 90 dias – Gera bloqueio de emissão de notas p o associado)*
Quitada (NF paga e repasses gerados)*
Quitada Parcial (valor parcial pago)*
Quitada Taxa (Taxa recebida – não gera repasse)*

     */
    indeterminada = -1,
    solicitada = 0,
    negada = 1,
    comentada = 2,
    gerada = 3,
    cancelada = 4,
    substituida = 5,
    pendente = 6,
    quitada = 7,
    quitadaParcial = 8,
    quitadaTaxa = 9,
    quitadaPendente = 10,
    devolvida = 11,
    assocSubstituida = 12,
    substituidaOriginal = 13,
        excluida = 14
}
export enum statusPagamento {
    /*
    Gerado (NF quitada)
Encaminhado (Depósito enviado ao banco)*
Pendente (Erro/falta de dados bancários, favor conferir cadastro)*
Efetuado (Comprovante de depósito recebido e confirmado)*

     */
    iniciado = 0,
    gerado = 1,
    encaminhado = 2,
    pendente = 3,
    efetuado = 4,
    pago = 5,
}
