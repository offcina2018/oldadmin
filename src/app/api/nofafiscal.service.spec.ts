import { TestBed, inject } from '@angular/core/testing';

import { NofafiscalService } from './nofafiscal.service';

describe('NofafiscalService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NofafiscalService]
    });
  });

  it('should be created', inject([NofafiscalService], (service: NofafiscalService) => {
    expect(service).toBeTruthy();
  }));
});
