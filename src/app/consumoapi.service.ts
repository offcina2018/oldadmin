import { Injectable } from '@angular/core';
import {Http, Jsonp, Response, Headers, RequestOptions, RequestOptionsArgs} from '@angular/http';
import { AppSettings} from './app.settings';

import { Observable } from 'rxjs/Observable';

import { IUrlOptions } from './odata.model';
import { RequestTypes } from './odata.model';
@Injectable()
export class ConsumoapiService {
    private url = AppSettings.API_ENDPOINT;

    public token: string;
    constructor(
        private host: string,
        private http: Http,
    ) {
        this.token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpZCI6MSwiaWF0IjoxNDk5MTk1MDI1LCJleHAiOjE0OTkyODE0MjV9.0nJ2zUCLmyMo7yM2NysZ5bKw8kkYbYop0C0qD5tInzHKERboD91lbzIbnyWw8CDOsqKMdSiuDytAKv5ExkQtYA';
    }


    private constructUrl(urlOptions: IUrlOptions): string {
        return this.url + urlOptions.restOfUrl;
    }
    private setBoundaries(pagina: number = 0, offset: number = 20 ) {
            let temp = '1=1&';
            if (pagina !== 0) {
                temp += 'page=' + pagina;
            }
            if (offset !== 20){
                temp += 'offset=' + offset;
            }
            return temp;

    }
        private constructHeader(): RequestOptionsArgs {
        const headers = new Headers({
            // 'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.token
        });
        const options: RequestOptions = new RequestOptions({headers: headers});
        return options;
    }
    // T specifies a generic output of function
    public Request<T>(requestType: RequestTypes,
                      urlOptions: IUrlOptions,
                      body?: any): Observable<T> {
        const options = this.constructHeader();
        let response: Observable<Response>;
        // True in case of post, put and patch
        if (body && options) {
            response = this.http[RequestTypes[requestType]](
                this.constructUrl(urlOptions),
                body,
                options);
        // True in case of post, put and patch if options is empty
        } else {
            if (body) {
                response = this.http[RequestTypes[requestType]](
                    this.constructUrl(urlOptions),
                    body);
            // True in case of get, delete, head and options
            } else {
                if (options) {
                    response = this.http[RequestTypes[requestType]](
                        this.constructUrl(urlOptions),
                        options);
                // True in case of get, delete, head and options, if options is empty
                } else {
                    response = this.http[RequestTypes[requestType]](
                        this.constructUrl(urlOptions),
                        options);
                }
            }
        }
        return response.map((res) => <T>res.json());
    }

}
