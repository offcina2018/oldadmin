/**
 * Created by paulomarinho on 07/06/17.
 */
import { Http } from '@angular/http';

import { ConsumoapiService } from './consumoapi.service';

export function provideODataService(url: string) {
    return {
        provide: ConsumoapiService, useFactory: (http) => {
            return new ConsumoapiService(url, http);
        },
        deps: [Http]
    };
}
