import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TesteComponent } from './teste.component';
import { FormsModule } from '@angular/forms';
import { routing } from './teste.routing';

@NgModule({
  imports: [
    CommonModule,
      FormsModule,
      routing
  ],
  declarations: [TesteComponent]
})
export class TesteModule { }
