/**
 * Created by paulomarinho on 28/05/17.
 */
import { Routes, RouterModule } from '@angular/router';

import { TesteComponent } from './teste.component';

const routes: Routes = [
    {
        path: '',
        component: TesteComponent
    }
];

export const routing = RouterModule.forChild(routes);
