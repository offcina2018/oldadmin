import { Injectable } from '@angular/core';
import {Http} from "@angular/http";
import {ApiLiame} from "./apiliame.service";
import {Page} from "./models/page";
import {PagedData} from "./models/paged-data";
import {NotafiscalModel} from "./models/notafiscal";
import { InfobancariaModel} from './models/infobancaria';
@Injectable()
export class InfobancariaService {

    constructor( public api: Http,
                 public apimaster: ApiLiame) {
    }
    listaInfoBancaria(page: Page){
        return this.apimaster.getLista('info_bancarias' , page)
            .do(console.log)
            .do(function(x){
                const pagedData = new PagedData<InfobancariaModel>();
                for (let i = 0; i < page.pageSize; i++) {
                    const  jsonObj = x[i];
                    // const employee = new AssociadoModel(jsonObj.id, jsonObj.email, jsonObj.Associadoz);
                    // pagedData.data.push(employee);
                }
                pagedData.page = new Page();
                console.log('ISSO Q TA VOLTANDO');
                console.log(pagedData.data);
                return pagedData.data;
            });
    }
    getInfoBancaria( id: number) {
        const options = this.apimaster.montaHeader();
        console.log(this.apimaster.url + '/info_bancarias');
        console.log( options);
        return this.api.get(this.apimaster.url + '/info_bancarias/' + id, options)
            .map(response => response.json().data);

    }
    insereInfoBancaria(alteracoes: any) {
        const options = this.apimaster.montaHeader();
        console.log( this.apimaster.url + '/info_bancarias/');
        console.log(options);
        return this.api.post(this.apimaster.url + '/info_bancarias/' , alteracoes , options)
            .map(response => response.json().data);

    }
    editaInfobancaria(id: number, alteracoes: any) {
        const options = this.apimaster.montaHeader();
        console.log( this.apimaster.url + '/info_bancarias/');
        console.log(options);
        return this.api.put(this.apimaster.url + '/info_bancarias/' + id, alteracoes, options)
            .map(response => response.json().data);
    }
    saveInfobancaria(id: number, alteracoes: any , idEntidade: number, tipoEntidade: string) {
        const options = this.apimaster.montaHeader();
        const urlAssociada = this.apimaster.url + '/' + tipoEntidade + '/' + idEntidade + '/info_bancaria/';
        if (id < 1) {
            return this.api.post(urlAssociada ,
                alteracoes,
                options)
                .map(response => response.json().data);
        } else {
            return this.editaInfobancaria(id, alteracoes);
        }
    }
    deletaInfobancaria(id) {
        const options = this.apimaster.montaHeader();

        return this.api.delete(this.apimaster.url + '/info_bancarias/' + id,  options)
            .map(response => response.json().data);
    }

}
