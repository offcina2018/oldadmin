import { Injectable } from '@angular/core';
import {Http} from "@angular/http";
import {ApiLiame} from "./apiliame.service";
import {Page} from "./models/page";
import {PagedData} from "./models/paged-data";
import {NotafiscalModel} from "./models/notafiscal";
import {DadosbrasilModel} from "./models/dadosbrasil";

@Injectable()
export class EmpresaService {

    constructor( public api: Http,
                 public apimaster: ApiLiame) {
    }
    listaNotas(page: Page){
        return this.apimaster.getLista('nota_fiscals' , page)
            .do(console.log)
            .do(function(x){
                const pagedData = new PagedData<DadosbrasilModel>();
                for (let i = 0; i < page.pageSize; i++) {
                    const  jsonObj = x[i];
                    // const  employee = new AssociadoModel(jsonObj.id, jsonObj.email, jsonObj.Associadoz);
                    // pagedData.data.push(employee);
                }
                pagedData.page = new Page();
                console.log('ISSO Q TA VOLTANDO');
                console.log(pagedData.data);
                return pagedData.data;
            });
    }
    getDados( id: number) {
        const options = this.apimaster.montaHeader();
        console.log(this.apimaster.url + '/empresa_dados_brasils');
        console.log( options);
        return this.api.get(this.apimaster.url + '/empresa_dados_brasils/' + id, options)
            .map(response => response.json().data);

    }
    insereDados(alteracoes: any) {
        const options = this.apimaster.montaHeader();
        console.log( this.apimaster.url + '/empresa_dados_brasils/');
        console.log(options);
        return this.api.post(this.apimaster.url + '/empresa_dados_brasils/' , alteracoes , options)
            .map(response => response.json().data);

    }
    editaDados(id: number, alteracoes: any) {
        const options = this.apimaster.montaHeader();
        console.log( this.apimaster.url + '/empresa_dados_brasils/');
        console.log(options);
        return this.api.put(this.apimaster.url + '/empresa_dados_brasils/' + id, alteracoes, options)
            .map(response => response.json().data);
    }
    saveDados(id: number, alteracoes: any) {
        if(id < 1 ) {
            console.log('vou salvar');
            return this.insereDados(alteracoes);
        }else {
            console.log('vou editar');
            return this.editaDados(id, alteracoes);
        }

    }
    deletaDados(id, alteracoes) {
        const options = this.apimaster.montaHeader();

        return this.api.delete(this.apimaster.url + '/empresa_dados_brasils/' + id,  options)
            .map(response => response.json().data);
    }

}
