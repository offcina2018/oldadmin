import {Injectable} from '@angular/core';
import { Http, Jsonp, Response, Headers, RequestOptions} from '@angular/http';

import { AppSettings} from '../app.settings';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Page } from './models/page';
import { PagedData } from './models/paged-data';
import { AssociadoModel } from './models/associado';
import { EnderecoModel} from './models/endereco';
import {IUrlOptions, tiposModelo} from './models/odata.model';
import { RequestTypes } from './models/odata.model';
import {EmpresaModel} from "./models/empresa";
import {AreaatuacaoModel} from "./models/areaatuacao";
import {PagamentoModel} from "./models/pagamento";

@Injectable()
export class ApiLiame {
  public url = AppSettings.API_ENDPOINT;
    idAssociado:number;
    idEmpresa: number;
    telFax: number;
    tel:number;
    fax:number;
    public token: string;
    public headers: RequestOptions;
    public ulrFinal: string;
    public corpo: string;
    private page: number = 1;
    private limit: number = 10;
    private sort: string = 'id ASC';
    busca = '';
    public x: IUrlOptions;
    // api: Http ;
    p: Observable<any>;
    private telefone_fax: any;
    constructor(
        public api: Http
   ) {
        if (localStorage.getItem('token')) {
            this.token = localStorage.getItem('token');
        }
  }
  montaHeader(): RequestOptions {
      // this.token = AppSettings.apitoken;
      console.log('TOKEN');
      console.log( this.token );
      const headers = new Headers({
          // 'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this.token
      });
      const options = new RequestOptions({
          headers: headers
      });
      return options;
  }
  getLista(urlLocal, page: Page) {
console.log('Pagina');
console.log(page);
        /*POPULAR RELACIONAMENTOS */
    let paramAdicionais = '';
    if (page.auxiliares.length > 0) {
      paramAdicionais += '&populate=';
      for (let i = 0; i < page.auxiliares.length; i++) {
        paramAdicionais += page.auxiliares[i] + ',';
       }
       paramAdicionais = paramAdicionais.substring(0, paramAdicionais.length - 1);
    }else {
      console.log('sem auxiliares');
    }

    /* DEFINIR CAMPOS DE RETORNO */
    let paramsRetorno = '';
    if (page.camposRetorno.length > 0) {
      paramsRetorno += '&fields=';
        for (let i = 0; i < page.camposRetorno.length; i++) {
            paramsRetorno += page.camposRetorno[i] + ',';
        }
        paramsRetorno = paramsRetorno.substring(0, paramsRetorno.length - 1);
    }



      /*FILTRO DE DATA*/
      // http://liame.org.br:3000/v1/associados?where={"data_nascimento":{
      // ">":"1978-01-01","<":"1987-01-01"
      // }
      // }&fields=data_nascimento

      let paramsDataFiltro = '';
      if (page.filtroData.campo.length > 0) {
          paramsDataFiltro = '&where={\"' + page.filtroData.campo + '\":{';
          if (page.filtroData.inicio !== null) {
              paramsDataFiltro += '\">\":\"' + page.filtroData.inicio + '\"';
          }
          if (page.filtroData.fim !== null) {
              if (page.filtroData.inicio !== null) paramsDataFiltro += ',';
              paramsDataFiltro += '\"<\":\"' + page.filtroData.fim + '\"';
          }
           paramsDataFiltro += '}';
        }

      /*FILTRO PARA SELECTS*/
      let paramsFixos = '';
      if (Object.keys(page.fixo).length > 0) {
          const c = Object.keys(page.fixo);
          // verifica se ja tem where
          if (paramsDataFiltro == '') {
              paramsFixos += '&where={';
          }else{
              paramsFixos += ',';
          }
          for (let i = 0; i < c.length; i++) {
              paramsFixos += '\"' + c[i] + '\" : \"' + page.fixo[c[i]] + '\",';
          }
          paramsFixos = paramsFixos.substring(0, paramsFixos.length - 1);
          console.log('params');
          console.log(paramsFixos);
      }
      if (paramsDataFiltro.length > 0  || paramsFixos.length > 0) {
          console.log('ta aqui FILTRO');
          paramsFixos += '}';
      }
      /*BUSCA TEXTUAL */
        if(page.valorBusca.length > 0) {
        let filtro = '';
            if (paramsDataFiltro == '') {
                filtro += '&where={\"or\":[';
            }else{
                filtro += ',';
            }
            //paramAdicionais = '&where={\"or\":[';
        // {"name":{"startsWith":"fred"}}]}
        for (let i = 0; i < page.camposBusca.length; i++) {
            filtro += '{\"' + page.camposBusca[i] + '\":{\"contains\":\"' +
                page.valorBusca + '\"}},';
        }
        filtro = filtro.substr(0, filtro.length - 1);
        filtro += ']}';
            paramsFixos += filtro;


    }
      const options = this.montaHeader();
      console.log('/' + urlLocal + '?' +
          'limit=' + page.pageSize +
          '&page=' + page.pageNumber +
          paramAdicionais +
          paramsRetorno +
          paramsDataFiltro +
          paramsFixos +
          '&sort=' + page.campoOrdenacao + ' ' + page.tipoOrdenacao );
      return this.Request(RequestTypes.get, '/' + urlLocal + '?' +
          'limit=' + page.pageSize +
          paramAdicionais +
          paramsRetorno +
          paramsDataFiltro +
          paramsFixos +
          '&page=' + page.pageNumber +
          // '&sort=' + page.campoOrdenacao + ' ' + page.tipoOrdenacao , '');
         '&sort=' + page.campoOrdenacao + ' '+ page.tipoOrdenacao, '');

  }
getEndereco(id: number){
   const options = this.montaHeader();
        console.log(this.url + '/enderecos');
        console.log( options);
        return this.api.get(this.url + '/enderecos/' + id, options)
            .map(response => response.json().data);
}
deletaEndereco(idEndereco){
        const options = this.montaHeader();
        console.log(this.url + '/empresas');
        console.log( options);
        return this.api.delete(this.url + '/enderecos/' + idEndereco, options)
            .map(response => response.json().data);
}
listaEnderecos(page: Page){
    return this.getLista('enderecos' , page)
            .do(console.log)
            .do(function(x){
                const pagedData = new PagedData<EnderecoModel>();
                for (let i = 0; i < page.pageSize; i++) {
                    const  jsonObj = x[i];
                    console.log(jsonObj);
                    // const  employee = new EnderecoModel(jsonObj);
                    // pagedData.data.push(employee);
                }
                pagedData.page = new Page();
                console.log('ISSO Q TA VOLTANDO2');
                console.log(pagedData.data);
                return pagedData.data;
            });
}

salvaEndereco(idEndereco , alteracoes: any, idEntidade: number, tipoEntidade: string) {
    const options = this.montaHeader();
    const urlAssociada = this.url + '/' + tipoEntidade + '/' + idEntidade + '/endereco/';
    if (idEndereco < 1) {
            return this.api.post(urlAssociada ,
                 alteracoes,
                 options)
                .map(response => response.json().data);
        } else {
        return this.api.put(urlAssociada + idEndereco ,
            alteracoes,
            options)
            .map(response => response.json().data);
        }
}
insereAreaAtuacao(alteracoes: any){
    const options = this.montaHeader();

    return this.api.post(this.url + '/area_atuacaos' ,
        alteracoes,
        options)
        .map(response => response.json().data);
}
    editaAreaAtuacao(idArea: number, alteracoes: any){
        const options = this.montaHeader();

        return this.api.put(this.url + '/area_atuacaos/'+ idArea ,
            alteracoes,
            options)
            .map(response => response.json().data);
    }


editaEndereco(idEndereco, alteracoes){
        const options = this.montaHeader();
        console.log( this.url + '/enderecos/');
        console.log(options);
        return this.api.put(this.url+ '/enderecos/' + idEndereco,alteracoes,options)
            .map(response => response.json().data);
}
insereEndereco(alteracoes: any){
    const options = this.montaHeader();
        console.log( this.url + '/enderecos/');
        console.log(options);
        return this.api.post(this.url+ '/enderecos/' ,alteracoes,options)
            .map(response => response.json().data);
}

    saveEmpresa(idEmpresa,alteracoes:any){
        const options = this.montaHeader();
        console.log( this.url + '/empresas/');
        console.log(options);
     return this.api.put(this.url+ '/empresas/' + idEmpresa,alteracoes,options)
            .map(response => response.json().data);

    }

    delEmpresa(idEmpresa) {
        const options = this.montaHeader();
        console.log(this.url + '/empresas');
        console.log( options);
        return this.api.delete(this.url + '/empresas/' + idEmpresa, options)
            .map(response => response.json().data);

    }

    delAssociado(idAssociado) {
        const options = this.montaHeader();
        console.log(this.url + '/associados');
        console.log( options);
        return this.api.delete(this.url + '/associados/' + idAssociado, options)
            .map(response => response.json().data);

    }
 /* montaLista<T>(url: string, modelo: tiposModelo,  pagina: Page, campos: any) {
      return this.getLista(url, pagina)
          .do(console.log)
          .do(function(x){
              const pagedData =  new PagedData[modelo];
              for (let i = 0; i < pagina.pageSize; i++) {
                  const  jsonObj = x[i];
                  const employee = new Object(modelo);
                  // const  employee = new modelo(jsonObj.id, jsonObj.email, jsonObj.Associadoz);
                  // pagedData.data.push(employee);
              }
              pagedData.page = new Page();
              return pagedData;
          });
  }*/

    getEmpresas(page: Page) {
        return this.getLista('empresas' , page)
            .do(console.log)
            .do(function(x){
                const pagedData = new PagedData<EmpresaModel>();
                for (let i = 0; i < page.pageSize; i++) {
                    const  jsonObj = x[i];
                    //const  employee = new EmpresaModel(jsonObj.id, jsonObj.nome, jsonObj.Empresaz);
                    //pagedData.data.push(employee);
                }
                pagedData.page = new Page();
                console.log('ISSO Q TA VOLTANDO2');
                console.log(pagedData.data);
                return pagedData.data;
            });

    }
    getEmpresa( id: number) {
        const options = this.montaHeader();
        console.log(this.url + '/empresas');
        console.log( options);
        return this.api.get(this.url + '/empresas/' + id, options)
            .map(response => response.json().data);

    }
    getAreaatuacao(id: number){
        const options = this.montaHeader();
        console.log(this.url + '/area_atuacaos');
        console.log( options);
        return this.api.get(this.url + '/area_atuacaos/' + id, options)
            .map(response => response.json().data);
    }
    salvaAreaAtuacao(idArea , alteracoes: any, idEntidade: number, tipoEntidade: string){
        if (!idEntidade || idEntidade < 1){
            if (idArea <1) {
                return this.insereAreaAtuacao(alteracoes);
            }else{
                return this.editaAreaAtuacao(idArea, alteracoes);
            }
        }else{
            return this.insereAreaAtuacao(alteracoes);
        }
    }
    listaAreaatuacao(page: Page) {
        return this.getLista('area_atuacaos' , page)
            .do(console.log)
            .do(function(x){
                const pagedData = new PagedData<AreaatuacaoModel>();
                for (let i = 0; i < page.pageSize; i++) {
                    const  jsonObj = x[i];
                    //const  employee = new AssociadoModel(jsonObj.id, jsonObj.email, jsonObj.Associadoz);
                    //pagedData.data.push(employee);
                }
                pagedData.page = new Page();
                console.log('ISSO Q TA VOLTANDO');
                console.log(pagedData.data);
                return pagedData.data;
            });

    }
    alteraEmpresa(ass: EmpresaModel): Boolean{
        return true;
    }

    listaPagamentos(page: Page){
        return this.getLista('pagamentos', page).do(console.log).do(function(x){
            const pagedData = new PagedData<PagamentoModel>();
            for (let i = 0; i < page.pageSize; i++) {
                console.log(x[i]);
                const  jsonObj = x[i];
                const  employee = new PagamentoModel();
                pagedData.data.push(employee);
            }
            pagedData.page = new Page();
            console.log('totalRes');
            console.log(x);
            pagedData.total = x.totalResultados;
            console.log('ISSO Q TA VOLTANDO');
            console.log(pagedData.data);
            return pagedData.data;
        });

    }


   getAssociados(page: Page) {
        console.log('getAss');
        console.log(page);
       return this.getLista('associados' , page)
           .do(console.log)
           .do(function(x){
               const pagedData = new PagedData<AssociadoModel>();
               for (let i = 0; i < page.pageSize; i++) {
                   const  jsonObj = x[i];
                   // const  employee = new AssociadoModel(jsonObj.id, jsonObj.email, jsonObj.Associadoz);
                   // pagedData.data.push(employee);
               }
               pagedData.page = new Page();
               console.log('totalRes');
               console.log(x);
               pagedData.total = x.totalResultados;
               console.log('ISSO Q TA VOLTANDO');
               console.log(pagedData.data);
               return pagedData.data;
           });

   }
    getAssociadosNovos(page: Page) {
        page.fixo = ['status=0'];
        return this.getLista('associados' , page)
            .do(console.log)
            .do(function(x){
                const pagedData = new PagedData<AssociadoModel>();
                for (let i = 0; i < page.pageSize; i++) {
                    const  jsonObj = x[i];
                    //const  employee = new AssociadoModel(jsonObj.id, jsonObj.email, jsonObj.Associadoz);
                    //pagedData.data.push(employee);
                }
                pagedData.page = new Page();
                console.log('ISSO Q TA VOLTANDO');
                console.log(pagedData.data);
                return pagedData.data;
            });

    }
   getCoordenadores(page: Page){
       page.valorBusca='is_coordenador=1' ;
       const options = this.montaHeader();
       console.log(this.url + '/associados/?' + page.valorBusca);
       console.log( options);
       return this.api.get(this.url + '/associados/?' + page.valorBusca, options)
           .map(response => response.json().data);


   }

   insereAssociado(alteracoes:any){
     const options = this.montaHeader();
        console.log( this.url + '/associados/');
        console.log(options);
        return this.api.post(this.url+ '/associados/' , alteracoes , options)
            .map(response => response.json().data);

   }
   editaAssociado(id:number,alteracoes:any){
        const options = this.montaHeader();
        console.log( this.url + '/associados/');
        console.log(options);
        return this.api.put(this.url+ '/associados/' + id,alteracoes,options)
            .map(response => response.json().data);
   }
    saveAssociado(id:number,alteracoes:any){
        if(id <1 ){
          return this.insereAssociado(alteracoes);
        }else{
          return this.editaAssociado(id, alteracoes);
        }

    }
   getAssociado( id: number) {
       const options = this.montaHeader();
       console.log(this.url + '/associados');
       console.log( options);
       return this.api.get(this.url + '/associados/' + id, options)
           .map(response => response.json().data);

   }
   getHistorico(id: number, modelo: string){
       const options = this.montaHeader();
       console.log(this.url + '/historicos/pegaPorModelo?id=' + id + '&modelo='+modelo);
       console.log( options);
       return this.api.get(this.url + '/historicos/pegaPorModelo?id=' +
           id + '&modelo=' + modelo, options)
           .map(response => response.json());
   }
   alteraAssociado(ass: AssociadoModel): Boolean{
        return true;
   }
// T specifies a generic output of function
    public Request(requestType: RequestTypes,
                      urlOptions: string,
                      body?: any) {
        const options = this.montaHeader();
        let response: Observable<Response>;
        // True in case of post, put and patch
        if (body && options) {
            response = this.api[RequestTypes[requestType]](
                this.constructUrl(urlOptions),
                body,
                options);
            // True in case of post, put and patch if options is empty
        } else {
            if (body) {
                response = this.api[RequestTypes[requestType]](
                    this.constructUrl(urlOptions),
                    body);
                // True in case of get, delete, head and options
            } else {
                if (options) {
                    response = this.api[RequestTypes[requestType]](
                        this.constructUrl(urlOptions),
                        options);
                    // True in case of get, delete, head and options, if options is empty
                } else {
                    response = this.api[RequestTypes[requestType]](
                        this.constructUrl(urlOptions),
                        options);
                }
            }
        }
        return response.map((res) => {
            console.log('retorno');
            console.log(res.json());
            return res.json();
            });
    }
    private constructUrl(urlOptions: string): string {
        return this.url + urlOptions;
    }


}
