/**
 * Created by paulomarinho on 18/08/17.
 */

import {AssociadoModel} from "./associado";
import {NotafiscalModel} from "./notafiscal";
import {EmpresaModel} from "./empresa";

export class PagamentoModel {
    valor: number;
    status: number;
    obs: string;
    taxa_adm: number;
    desconto: number;
    acrescimo: number;
    data_pagamento: Date;
    data_quitacao: Date;
    idpagamento: string;
    associado: AssociadoModel;
    empresa: EmpresaModel;
    nfs: NotafiscalModel;
    id: number;
    createdAt: Date;
    updatedAt: Date;
}
