/**
 * Created by paulomarinho on 27/08/17.
 */
export class DadosbrasilModel {
    empresa?: number;
    cnpj_cpf?: string;
    is_cnpj?: boolean;
    insc_estadual?: string;
    insc_municipal?: string;
    id: number;
    createdAt?: Date;
    updatedAt?: Date;
}
