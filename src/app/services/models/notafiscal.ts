/**
 * Created by paulomarinho on 25/08/17.
 */
import {AssociadoModel} from "./associado";
import {EmpresaModel} from "./empresa";
import {AreaatuacaoModel} from "./areaatuacao";
/**
 * Created by paulomarinho on 18/08/17.
 */
export class NotafiscalModel {
    id: number;
    valor?: number;
    valor_taxa?: number;
    associado?: AssociadoModel;
    empresa?: EmpresaModel;
    descricao?: string;
    numero?: number;
    is_quitada?: boolean;
    obs?: string;
    createdAt?: Date;
    updatedAt?: Date;
    statusnf: number;



}
