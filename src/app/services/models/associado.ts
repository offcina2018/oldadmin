/**
 * Created by paulomarinho on 04/06/17.
 */
 import { EnderecoModel } from './endereco';
 import {infoBancariaModel} from '../../../service/model/infobancaria';
export class AssociadoModel {
    coordenador: AssociadoModel;
    assinatura: number;
    user: number;
    nome: string;
    nome_artistico: string;
    data_nascimento: any;
    tipo: number;
    identidade: string;
    orgao_expedidor: string;
    nome_responsavel?: any;
    cpf: string;
    orgao_expedidor_responsavel?: any;
    identidade_responsavel?: any;
    cpf_responsavel?: any;
    is_coordenador: boolean;
    status: number;
    taxa_adm_porcentagem: number;
    id: number;
    createdAt?: any;
    updatedAt?: any;
    endereco?: EnderecoModel;
    infobancaria?: infoBancariaModel;
    constructor( ) {
        // this.coordenador = coordenador;
        // this.assinatura = assinatura;
        // this.user = user;
        // this.nome = nome;
        // this.nome_artistico=nome_artistico;
        // this.data_nascimento = data_nascimento;
        // this.tipo=tipo;
        // this.identidade=identidade;
        // this.orgao_expedidor=orgao_expedidor;
        // this.nome_responsavel = nome_responsavel;
        // this.cpf = cpf;
        // this.orgao_expedidor_responsavel=orgao_expedidor_responsavel;
        // this.identidade_responsavel=identidade_responsavel;
        // this.cpf_responsavel = cpf_responsavel;
        // this.is_coordenador = is_coordenador;
        // this.status = status;
        // this.taxa_adm_porcentagem=taxa_adm_porcentagem;
        // this.id=id;
        // this.createdAt=createdAt;
        // this.updatedAt = updatedAt;
    }
}
