import {AssociadoModel} from "./associado";
import {EmpresaModel} from "./empresa";
/**
 * Created by paulomarinho on 18/08/17.
 */
export class AreaatuacaoModel {
    nome?: string;
    is_ativo?: boolean;

    id?: number;
    is_sub_item?: AreaatuacaoModel;
    createdAt?: any;
    updatedAt?: any;
    associado?: AssociadoModel;
    empresa?: EmpresaModel;
}
