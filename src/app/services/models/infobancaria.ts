/**
 * Created by paulomarinho on 27/08/17.
 */
export class InfobancariaModel {
    cod_banco?: string;
    titular?: string;
    agencia?: string;
    conta?: string;
    tipo_conta?: string;
    is_conjunta?: boolean;
    obs?: string;
    tempid?: number;
    id: number;
    createdAt?: any;
    updatedAt?: any;
}
