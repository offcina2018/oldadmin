
/**
 * Created by paulomarinho on 04/06/17.
 */
export class Page {
    /* campos realmente necessários */
    // The number of elements in the page
    size = 40;
    // The total number of elements
    totalElements = 4000;
    // The total number of pages
    totalPages = 100;
    // The current page number
    pageNumber = 0;
    pageSize = 10;
    number = 3;
    /* personalização para Liame */

    /*ordenacao*/
    campoOrdenacao = 'createdAt';
    tipoOrdenacao = 'DESC';

    /* busca aberta, RIP :(*/
    valorBusca = '';
    camposBusca = [];
    /* campos para serem populados de
    tabelas auxiliares
     */
    auxiliares = [];

    /* criando filtros de relacionadas*/
    fixo: {[k: string]: any} = {};

    /* filtros de data*/
    camposRetorno = [];
    filtroData: {
        campo: String ,
        inicio: Date,
        fim: Date
    } = {campo: '', inicio: null, fim: null};
}
