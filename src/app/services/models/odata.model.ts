/**
 * Created by paulomarinho on 07/06/17.
 */
import {AssociadoModel} from './associado';
import {EmpresaModel} from "./empresa";
export enum RequestTypes {
    get,
    post,
    put,
    delete,
    patch,
    head,
    options
}

export interface IUrlOptions {
    restOfUrl: string;
}

export enum tiposModelo{
    AssociadoModel = AssociadoModel,
    EmpresaModel = EmpresaModel
}
