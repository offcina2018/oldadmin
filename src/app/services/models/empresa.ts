/**
 * Created by offcina on 03/07/17.
 */
export class EmpresaModel {

    // CTRL+C + CTRL+V DO ASSOCIADOS. TUDO PORQUE QUERIA TESTAR A LISTA!

    Empresaz?: any;
    Estadoz?: any;
    nome?: string;
    id_empresa?: any;
    id_estado?: any;
    id_usuario?: any;
    cnpj_cpf?: any;
    razao_social?: any;
    inscricao_estadual?: any;
    inscricao_municipal?: any;
    email?: any;
    telefone?: any;
    telefone_fax?: any;
    endereco?: any;
    endereco_cidade?: any;
    endereco_bairro?: any;
    endereco_numero?: any;
    endereco_cep?: any;
    endereco_complemento?: any;
    observacao?: any;
    is_estrangeira?: any;
    status?: any;
    createdAt?: any;
    updatedAt?: any;
    id: number;
    constructor(id: number,
                Empresaz: any,
                Estadoz?: any,
                id_empresa?: any,
                id_estado?: any,
                id_usuario?: any,
                cnpj_cpf?: any,
                razao_social?: any ,
                inscricao_estadual?: any,
                inscricao_municipal?: any,
                email?: any,
                nome?: string,
                telefone?: any,
                telefone_fax?: any,
                endereco?: any,
                endereco_cidade?: any,
                endereco_bairro?: any,
                endereco_numero?: any,
                endereco_cep?: any,
                endereco_complemento?: any,
                observacao?: any,
                is_estrangeira?: any,
                status?: any,
                createdAt?: any,
                updatedAt?: any,
     ) {
        this.Empresaz =  Empresaz;
        this.Estadoz = Estadoz;
        this.id_empresa = id_empresa;
        this.id_estado = id_estado;
        this.id_usuario = id_usuario;
        this.cnpj_cpf = cnpj_cpf;
        this.razao_social = razao_social;
        this.inscricao_estadual = inscricao_estadual;
        this.inscricao_municipal =  inscricao_municipal;
        this.email = email;
        this.nome = nome;
        this.endereco = endereco;
        this.endereco_cidade = endereco_cidade;
        this.endereco_bairro = endereco_bairro;
        this.endereco_numero = endereco_numero;
        this.endereco_cep = endereco_cep;
        this.endereco_complemento = endereco_complemento;
        this.telefone = telefone;
        this.telefone_fax = telefone_fax;
        this.observacao = observacao;
        this.status = status;
        this.is_estrangeira = is_estrangeira;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.id = id;
    }




}
