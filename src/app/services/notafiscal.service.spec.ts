import { TestBed, inject } from '@angular/core/testing';

import { NotafiscalService } from './notafiscal.service';

describe('NotafiscalService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NotafiscalService]
    });
  });

  it('should be created', inject([NotafiscalService], (service: NotafiscalService) => {
    expect(service).toBeTruthy();
  }));
});
