import { Injectable } from '@angular/core';
import {Http} from "@angular/http";
import {ApiLiame} from "./apiliame.service";
import {Page} from "./models/page";
import {PagedData} from "./models/paged-data";
import {NotafiscalModel} from "./models/notafiscal";

@Injectable()
export class NotafiscalService {

  constructor( public api: Http,
                public apimaster: ApiLiame) {
  }
    listaNotas(page: Page) {
      console.log('page', page);
      page.tipoOrdenacao = 'desc';
        return this.apimaster.getLista('nota_fiscals' , page).do(console.log)
            .do(function(x){
                const pagedData = new PagedData<NotafiscalModel>();
                for (let i = 0; i < page.pageSize; i++) {
                    const  jsonObj = x[i];
                    const  employee = new NotafiscalModel();
                    pagedData.data.push(employee);
                }
                pagedData.page = new Page();
                console.log('totalRes');
                console.log(x);
                pagedData.total = x.totalResultados;
                console.log('ISSO Q TA VOLTANDO');
                console.log(pagedData.data);
                return pagedData.data;
            });
    }
    getNota( id: number) {
        const options = this.apimaster.montaHeader();
        console.log(this.apimaster.url + '/nota_fiscals/' + id + '?populate=associados');
        console.log( options);
        return this.api.get(this.apimaster.url + '/nota_fiscals/' + id + '?populate=associados', options)
            .map(response => response.json().data);

    }
    deletaNota(id: any){
        const options = this.apimaster.montaHeader();
        console.log( this.apimaster.url + '/nota_fiscals/');
        console.log(options);
        return this.api.delete(this.apimaster.url + '/nota_fiscals/'+id  , options)
            .map(response => response.json().data);
    }
    insereNota(alteracoes: any) {
        const options = this.apimaster.montaHeader();
        console.log( this.apimaster.url + '/nota_fiscals/');
        console.log(options);
        return this.api.post(this.apimaster.url + '/nota_fiscals/' , alteracoes , options)
            .map(response => response.json().data);

    }
    insereAssociadoNota(id: number, idassociado: number) {
        const options = this.apimaster.montaHeader();
        console.log( this.apimaster.url + '/nota_fiscals/'+ id + '/associados/'+ idassociado);
        console.log(options);
        return this.api.post(this.apimaster.url + '/nota_fiscals/'+ id + '/associados/'+ idassociado, null , options)
            .map(response => response.json().data);

    }
    removeAssociadoNota(id: number, idassociado: number) {
        const options = this.apimaster.montaHeader();
        console.log( this.apimaster.url + '/nota_fiscals/'+ id + '/associados/'+ idassociado);
        console.log(options);
        return this.api.delete(this.apimaster.url + '/nota_fiscals/'+ id + '/associados/'+ idassociado , options)
            .map(response => response.json().data);

    }
    editaNota(id: number, alteracoes: any) {
        const options = this.apimaster.montaHeader();
        console.log( this.apimaster.url + '/nota_fiscals/');
        console.log(options);
        return this.api.put(this.apimaster.url + '/nota_fiscals/' + id, alteracoes, options)
            .map(response => response.json().data);
    }
    /*saveNota(id: number, alteracoes: any) {
        if(id <1 ) {
            return this.insereNota(alteracoes);
        }else{
            return this.editaNota(id, alteracoes);
        }

    }*/
    saveNota(id: number, alteracoes: any , idEntidade: number, tipoEntidade: string) {
        const options = this.apimaster.montaHeader();
        const urlAssociada = this.apimaster.url + '/' + tipoEntidade + '/' + idEntidade + '/nfs/';
        if (id < 1) {
            return this.api.post(urlAssociada ,
                alteracoes,
                options)
                .map(response => response.json().data);
        } else {
            return this.editaNota(id, alteracoes);
        }
    }
}
