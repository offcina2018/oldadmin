import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Router} from "@angular/router";
import {AppSettings} from "../../../app.settings";
import {Http} from "@angular/http";
import {ApiLiame} from "../../../services/apiliame.service";

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    novosAssociados: number = 0;
    novasNotas: number = 0;
    novasEmpresas: number = 0;
    avisoNovos: number = 0;
    usuario: any;
    constructor(private translate: TranslateService,
                public router: Router,
                public http: Http,
    public apiliame: ApiLiame) {
        if (localStorage.getItem('user')) {
            this.usuario = JSON.parse(localStorage.getItem('user'));
            console.log('USUARIO');
            console.log(this.usuario);
        }
        const header = this.apiliame.montaHeader();
        this.http.get(AppSettings.API_ENDPOINT + '/associados?status=0', header).subscribe( ret => {
            const retorno = ret.json();
            this.novosAssociados = retorno.totalResultados;
            this.avisoNovos += this.novosAssociados;
            console.log(ret.json());
        });
        this.http.get(AppSettings.API_ENDPOINT + '/nota_fiscals?status=0', header).subscribe( ret => {
            const retorno = ret.json();
            this.novasNotas = retorno.totalResultados;
            this.avisoNovos += this.novasNotas;
            console.log(ret.json());
        });
        this.http.get(AppSettings.API_ENDPOINT + '/empresas?status=0', header).subscribe( ret => {
            const retorno = ret.json();
            this.novasEmpresas = retorno.totalResultados;
            this.avisoNovos += this.novasEmpresas;
            console.log(ret.json());
        });
    }

    ngOnInit() {}

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('push-right');
    }

    rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    onLoggedout() {
        console.log('REMOVENDO');
        localStorage.removeItem('isLoggedin');
        localStorage.removeItem('user');
        localStorage.removeItem('token');
        this.router.navigate(['/login']);
    }

    changeLang(language: string) {
        this.translate.use(language);
    }
    logout() {
        if (localStorage.getItem('user')) {
            console.log( 'REMOVENDO');
            localStorage.removeItem('user');
            localStorage.removeItem('token');
            this.router.navigate(['/login']);
        }
    }
    login() {

    }
}
