import { Component } from '@angular/core';
import {Routes, RouterModule, Router} from '@angular/router';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {
    isActive = false;
    showMenu = '';
    constructor( public router: Router) {}

    eventCalled() {
        this.isActive = !this.isActive;
    }
    addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }
    logout() {
        if (localStorage.getItem('user')) {
            console.log( 'REMOVENDO');
            localStorage.removeItem('user');
            localStorage.removeItem('token');
            this.router.navigate(['/login']);
        }
    }

}
