import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'statusNf'
})
export class StatusNfPipe implements PipeTransform {

    transform(value: any, args?: any): any {
        const keys = [
            'solicitada',
            'negada',
            'comentada' ,
            'gerada',
            'cancelada',
            'substituida',
            'pendente',
            'quitada',
            'quitadaParcial',
            'quitadaTaxa',
            'quitadaPendente',
            'devolvida',
            'assocSubstituida',
            'substituidaOriginal',
            'excluida',

        ];
        return keys[value];
    }
    // transform(value, args: string[]): any {
    //     const keys = [];
    //     for (const enumMember in value) {
    //         if (!isNaN(parseInt(enumMember, 10))) {
    //             keys.push({key: enumMember, value: value[enumMember]});
    //             // Uncomment if you want log
    //             // console.log("enum member: ", value[enumMember]);
    //         }
    //     }
    //     return keys;
    // }

}
