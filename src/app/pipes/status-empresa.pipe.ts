import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'statusEmpresa'
})
export class StatusEmpresaPipe implements PipeTransform {

  transform(value: any, args?: any): any {
      const estadosAssociado = [
          'Pendente',
          'Aprovado',
          'Negado' ,
          'Cancelado'
      ];
      return estadosAssociado[value];
  }

}
