import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'estaQuitada'
})
export class EstaQuitadaPipe implements PipeTransform {

  transform(value: any, args?: any): any {
      if (value == '0') {
          return "Quitada";
      }else {
          return "Não quitada";
      }
  }

}
