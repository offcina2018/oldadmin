import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'statusAssociado',

})
export class StatusAssociadoPipe implements PipeTransform {

  transform(value: any, args?: any): any {
      const estadosAssociado = [
          'Pendente',
          'Aprovado',
          'Negado' ,
          'Cancelado'
      ];
    return estadosAssociado[value];
  }

}
