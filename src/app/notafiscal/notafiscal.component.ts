import {Component, OnInit} from '@angular/core';
import {Page} from "../services/models/page";
import {AuthService} from "../auth.service";
import {Router} from "@angular/router";
import {ApiLiame} from "../services/apiliame.service";
import { NotafiscalService} from '../services/notafiscal.service';
import {Http} from "@angular/http";
import {NotafiscalModel} from "../services/models/notafiscal";
import {  ViewEncapsulation, ViewChild } from '@angular/core';
import {statusNotaFiscal} from "../app.settings";
import { StatusNfPipe} from "../pipes/status-nf.pipe";

@Component({
  selector: 'app-notafiscal',
  templateUrl: './notafiscal.component.html',
  styleUrls: ['./notafiscal.component.scss'],
    providers: [NotafiscalService, NotafiscalModel, StatusNfPipe],
    encapsulation: ViewEncapsulation.None
})
export class NotafiscalComponent implements OnInit {
    @ViewChild('myTable') table: any;
    public statusNotaFiscal = statusNotaFiscal;
    paginacao: any = {
        limit: 0,
        totalResultados: 0,
        paginaAtual: 1
    };
    loading = false;
    token: string;
    busca: string;
    public celular = ['(', /[1-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
    temp = [];
    rota: Router;
    public alerta: any = [];
    url: string;
    handleError: any;
    notas= []; // : Array<AssociadoModel>
    errorMessage: any;
    page = new Page();
    autenticador: AuthService;
    linhas: Array<NotafiscalModel> = [];
    lstEmpresas: any;
    lstAssociados: any;
    idLstAssociado: any;
    idLstEmpresa: any;
    dtLstInicio: Date;
    dtLstFim: Date;
    keys: any;
    statusNFiscal: any;
    constructor( public api: NotafiscalService,
                 public api2: Http,
                 public api3: ApiLiame,
                 public auth: AuthService,
                 public router: Router ) {
        /* seta variaveis iniciais */
        this.page.pageNumber = 0;
        this.page.auxiliares = ['associados', 'nfs', 'conta', 'endereco', 'empresa' ];
        this.page.campoOrdenacao = 'id';
        this.page.tipoOrdenacao = 'desc';
        this.rota = router;
        this.autenticador = auth;
        this.page.size = 20;
        this.comboEmpresas();
        this.comboAssociados();
        this.keys = Object.keys(this.statusNotaFiscal).filter(Number);
        const s = Object.keys(statusNotaFiscal);

    };
       comboEmpresas() {
           const page = new Page();
           page.pageSize = 5000;
           page.pageNumber = 0;
           page.camposRetorno = ['id', 'nome_fantasia'];
           page.campoOrdenacao = 'nome_fantasia';
           page.tipoOrdenacao = 'asc';
           this.api3.getEmpresas(page).subscribe(ret =>{
               this.lstEmpresas = ret.data;
               console.log('getEmpresas');
       });
   }
    changeLstEmpresas(x) {
        if (x.value === 0) {
            delete       this.page.fixo.empresa;
        }else {
            this.page.fixo.empresa = x.value;
        }
        this.setPage({ offset: 0});
        console.log(x.value);
    }
   comboAssociados() {
       const page = new Page();
       page.pageSize = 5000;
       page.pageNumber = 0;
       page.camposBusca = [ ' numero'];
       page.camposRetorno = ['id', 'nome'];
       page.campoOrdenacao = 'nome';
       page.tipoOrdenacao = 'asc';
       this.api3.getAssociados(page).subscribe(ret => {
           this.lstAssociados = ret.data;
           console.log('getAssociados');
       });
  }
    changeLstAssociados(x) {
        if (x.value == 0) {
            delete this.page.fixo.associado;
        }else {
            this.page.fixo.associado = x.value;
        }

        this.setPage({ offset: 0 });
        console.log(x.value);
    }
    changeLstStatus(x) {
        if (x.value == 0) {
            delete this.page.fixo.statusnf;
        }else {
            this.page.fixo.statusnf = x.value;
        }

        this.setPage({ offset: 0 });
        console.log(x.value);
    }
    changeDataInicio(x) {
      // http://liame.org.br:3000/v1/associados?where={"data_nascimento":{">":"1978-01-01"}}&fields=data_nascimento
        // FINAL
        // http://liame.org.br:3000/v1/associados?where={"data_nascimento":{">":"1978-01-01","<":"1987-01-01"}}&fields=data_nascimento
      this.page.filtroData.campo = 'createdAt';
      this.page.filtroData.inicio = x.value;
      this.setPage({ offset: 0 });

        // this.page.fixo.datainicio = x.value;
    }
    changeDataFim(x) {
      // http://liame.org.br:3000/v1/associados?where={"data_nascimento":{">":"1978-01-01"}}&fields=data_nascimento
        // FINAL
        // http://liame.org.br:3000/v1/associados?where={"data_nascimento":{">":"1978-01-01","<":"1987-01-01"}}&fields=data_nascimento
      this.page.filtroData.campo = 'createdAt';
      this.page.filtroData.fim = x.value;
      this.setPage({ offset: 0 });

        // this.page.fixo.datainicio = x.value;
    }
    ngOnInit() {
        if (!this.autenticador.checkLogin()) {
            this.rota.navigate(['/dashboard']);
        }
        this.setPage({ offset: 0 , });
    };

    /**
     * Navega para detalhes
     * @param id id do associado
     */
    detalhes(id) {
        /* navega para detalher */
        this.router.navigate(['/notasfiscais/perfil', id]);
    }
    detalhe(id: number) {
        /* navega para detalher */
        this.router.navigate(['/notasfiscais/perfil', id]);
    }
    /**
     * Novo cadastro
     */
    irParaCadastro() {
        /* navega para detalher */
        this.router.navigate(['/notasfiscais/perfil', 0]);
    }

    cadastrarEnderecoUsuario(iduser, id) {
        this.router.navigate(['/enderecos/associados/' + iduser + '/endereco', id ]);
    }
    /**
     * PM - NAO VALE
     * @param event
     */
    filtrar() {

        this.page.valorBusca = this.busca;
        console.log('updateFilter');
        console.log(event);

        this.api.listaNotas(this.page).subscribe(
            // pagedData =>  this.rows = pagedData,
            pagedData =>  this.linhas = pagedData.data,
        );
    }

    /**
     * PM - NAO VALE
     */
    atualizaFiltro() {

    }

    /**
     * ORDENACAO
     * @param event evento
     */
    onSort(event) {

        if (event) {
            this.page.tipoOrdenacao = event.sorts;
            this.page.campoOrdenacao = event.sorts[0].prop;
    }
    console.log(this.page);
        this.page.pageNumber = 0;
        this.loading = true;
        this.api.listaNotas(this.page).subscribe(
            // pagedData =>  this.rows = pagedData,
            pagedData =>  {
                this.linhas = pagedData.data;
                this.paginacao = pagedData;
                console.log('listaNotas210');
                console.log(pagedData);
            }
        );
        this.notas = this.linhas;

        this.loading = false;
    }



    /**
     * Paginacao
     * @param page Pagina
     */
    setPage(pageInfo) {

        this.page.pageNumber = pageInfo.offset;
        this.api.listaNotas(this.page).subscribe(
            pagedData =>  {
                this.linhas = pagedData.data;
                this.paginacao = pagedData;
                console.log('listaNotas234');
                console.log(pagedData);
            }
            // pagedData =>  this.associados = pagedData,
        );
        this.notas = this.linhas;
    }

    /**
     * Abre Linha
     * @param row numero da linha
     */
    toggleExpandRow(linha) {
        console.log('Toggled Expand Row!', linha);
        this.table.rowDetail.toggleExpandRow(linha);
    }

    onDetailToggle(event) {
        console.log('Detail Toggled', event);
    }


}
