import { RequestOptions, Headers, Http } from '@angular/http';
import {AppSettings, statusNotaFiscal} from '../app.settings';
// import {Router} from "@angular/router";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ApiLiame } from '../services/apiliame.service';
import { EnderecoModel } from '../services/models/endereco';
import {Page} from '../services/models/page';
import {ViewChild, OnInit, ViewEncapsulation, Component, Input} from '@angular/core';
import {Form, FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';
import {NotificationService} from 'ng2-notify-popup';
import {NotafiscalModel} from '../services/models/notafiscal';
import {NotafiscalService} from '../services/notafiscal.service';
// import {statusNotaFiscal} from "../app.settings";

/**
 * Created by offcina on 28/06/17.
 */
@Component({
    selector: 'app-notafiscal',
    templateUrl: './detalhes.notafiscal.html',
    styleUrls: ['./notafiscal.component.scss'],
    providers: [ApiLiame, NotafiscalModel, DatePipe, NotificationService , NotafiscalService],
    encapsulation: ViewEncapsulation.None
})
export class DetalhesNotaFiscalComponent implements OnInit {

    /* propriedades */
    @ViewChild('myTable') table: any;
    @Input() ass: Observable<EnderecoModel>;
    loading = false;
    token: string;
    public celular = ['(', /[1-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
    temp = [];
    rota: Router;
    public alerta: any = [];
    coordenadores: any;
    url: string;
    handleError: any;
    // rows = [];
    idNotaFiscal: any;
    idEntidade: any;
    tipoEntidade: any;
    podeEditar: boolean;
    errorMessage: any;
    page = new Page();
    rows: Array<NotafiscalModel> = [];
    meuForm: any;
    listaAssociados: any;
    lstAssociados: any;
    idLstAssociado: number;
    fmBuilder: FormBuilder;
    stsNF: statusNotaFiscal;
    public historico: any = [];
    public nfs: any = {};
    public statusNotaFiscal = statusNotaFiscal;
    keys: any;
    contas: any;
    associado: any;
    statusNFiscal: any;

    constructor(private route: ActivatedRoute,
                private router: Router,
                public api: ApiLiame ,
                public apinf: NotafiscalService,
                public api2: Http,
                private fb: FormBuilder,
                private notafiscal: NotafiscalModel,
                public notificationService: NotificationService,
                ) {

        /* setup form */
        this.keys = Object.keys(this.statusNotaFiscal).filter(Number);
        this.podeEditar = false;

        this.meuForm = new FormGroup({
            descricao: new FormControl ({value: '00000', disabled: true}, Validators.required),
            valor: new FormControl ({value: '', disabled: true}, Validators.required),
            valor_taxa: new FormControl ({value: '', disabled: true}, Validators.required),
            numero: new FormControl ({value: '', disabled: true}, Validators.required),
            is_quitada: new FormControl ({value: '', disabled: true}, Validators.required),
            obs : new FormControl({value: '', disabled: true}, Validators.required),
            razao_social_gerada : new FormControl({value: '', disabled: true}, Validators.required),
            statusnf: new FormControl ({value: '', disabled: true}, Validators.required),
            associados : new FormControl({value: '', disabled: true}, Validators.required),
            empresa : new FormControl({value: '', disabled: true}, Validators.required),
            id : new FormControl('', Validators.required),
            // auditor : new FormControl('', Validators.required),
            createdAt : new FormControl('', Validators.required),
            updatedAt : new FormControl('', Validators.required),

        });
        this.route.params
            .subscribe(params => {
                this.idNotaFiscal =  params['id'];
                this.idEntidade =  params['identidade'];
                this.tipoEntidade =  params['tipoentidade'];
                if (this.idEntidade ) {
                    this.meuForm.controls[this.tipoEntidade].setValue(this.idEntidade);
                }
                console.log(' sou do time ' + this.idNotaFiscal);

                if (this.idNotaFiscal > 0 ) {
                    console.log(' poe a mao na cabeca');
                    this.apinf.getNota(this.idNotaFiscal)
                        .subscribe((ret: any) => {

                            (<FormGroup>this.meuForm).setValue(ret, {onlySelf: true});
                            this.listaAssociados = ret.associados;
                            console.log('esses sao os assoc');
                            console.log(this.listaAssociados);
                            this.idNotaFiscal = ret.id;
                            this.nfs = ret ;
                            this.associado = ret.associado ;
                            this.contas = ret.contas ;

                        });
                }
                console.log(' que vai comecar');

                /*this.api.getHistorico(this.idNotaFiscal, 'nota_fiscal').subscribe(ret => {
                    this.historico = ret;
                    console.log('hist');
                    console.log(this.historico);
                    console.log(ret);
                });*/
            });
        // this.comboAssociados();

        // this.statusNFiscal
        const s = Object.keys(statusNotaFiscal);
        this.statusNFiscal = s.slice(s.length / 2);
    }

    ngOnInit() {


    };
    editar() {
        const ctrl = this.meuForm.get('descricao');
        ctrl.enabled ? ctrl.disable() : ctrl.enable();
        const ctrl1 = this.meuForm.get('valor');
        ctrl1.enabled ? ctrl1.disable() : ctrl1.enable();
        const ctrl2 = this.meuForm.get('valor_taxa');
        ctrl2.enabled ? ctrl2.disable() : ctrl2.enable();
        const ctrl3 = this.meuForm.get('numero');
        ctrl3.enabled ? ctrl3.disable() : ctrl3.enable();
        const ctrl4 = this.meuForm.get('is_quitada');
        ctrl4.enabled ? ctrl4.disable() : ctrl4.enable();
        const ctrl5 = this.meuForm.get('obs');
        ctrl5.enabled ? ctrl5.disable() : ctrl5.enable();
        const ctrl6 = this.meuForm.get('razao_social_gerada');
        ctrl6.enabled ? ctrl6.disable() : ctrl6.enable();
        const ctrl7 = this.meuForm.get('statusnf');
        ctrl7.enabled ? ctrl7.disable() : ctrl7.enable();
        const ctrl8 = this.meuForm.get('associados');
        ctrl8.enabled ? ctrl8.disable() : ctrl8.enable();
        const ctrl9 = this.meuForm.get('empresa');
        ctrl9.enabled ? ctrl9.disable() : ctrl9.enable();
        // const ctrl10 = this.meuForm.get('id');
        // ctrl10.enabled ? ctrl10.disable() : ctrl10.enable();
        // this.podeEditar = !this.podeEditar;
        // console.log(this.podeEditar);
        //
        // if (this.podeEditar === false) {
        //     this.podeEditar = true;
        //     // console.log(this.podeEditar);
        // }else {
        //     this.podeEditar = false;
        //     // console.log(this.podeEditar);
        // }
    }
    salvaNotaFiscal() {
        /* salva endereco pela entidade relacionada */
        this.alerta.avisoSucesso = '';
        this.meuForm.removeControl('updatedAt');
        this.meuForm.removeControl('createdAt');
        this.meuForm.removeControl('empresa');
        this.apinf.saveNota(this.idNotaFiscal, this.meuForm.value, this.idEntidade, this.tipoEntidade).subscribe(x => {
            this.notificationService.show('Salvo', {
                position: 'bottom',
                duration: '2000',
                type: 'success' });
            // this.router.navigate(['/empresas']);
        });
    }
    deletaNotaFiscal(id) {
        /* deleta endereco pela entidade relacionada */
        console.log('deleta ende');
        this.alerta.avisoSucesso = '';

        this.apinf.deletaNota(id).do(x => {
                this.notificationService.show('Salvo', {
                    position: 'bottom',
                    duration: '2000',
                    type: 'success' });
                // this.setPage(1);
                // this.router.navigate(['/empresas']);

            },
            erro => {
                this.notificationService.show('Erro', {
                    position: 'bottom',
                    duration: '2000',
                    type: 'success' });
            })
            .catch(this.handleError)
            .subscribe((ok) => {
                console.log(ok);
            });
        this.router.navigate(['/notafiscal']);


    }
    comboAssociados() {
        console.log(' o rebolation');

        let page = new Page();
        page.pageSize = 5000;
        page.pageNumber = 0;
        page.camposBusca = [ ' numero'];
        page.camposRetorno = ['id', 'nome'];
        page.campoOrdenacao = 'nome';
        page.tipoOrdenacao = 'asc';
        this.api.getAssociados(page).subscribe(ret =>{
            this.lstAssociados = ret;
            console.log('leu');
        });
    }
    changeLstAssociados(x) {
            this.idLstAssociado = x.value;
    }
    adicionarNFAss(){
        if(this.idLstAssociado != 0){
            this.apinf.insereAssociadoNota(this.idNotaFiscal, this.idLstAssociado).subscribe(ret =>{
                // this.lstAssociados = ret;
                console.log('leu');
            });
        }
    }
    removeNFAss(idAss: number){
        if(idAss != 0){
            this.apinf.removeAssociadoNota(this.idNotaFiscal, idAss).subscribe(ret =>{
                // this.lstAssociados = ret;
                console.log('leu');
            });
        }
    }
    voltaTipo(){
        this.router.navigate(['/notasfiscais']);
    }
    detalhesAss(id){
        this.router.navigate(['/associado/perfil/'+id]);

    }
    loga(event){
        console.log(event);
    }
}

