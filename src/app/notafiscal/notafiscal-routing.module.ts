import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotafiscalComponent } from './notafiscal.component';
import {AuthGuard} from "../shared/guard/auth.guard";
import { DetalhesNotaFiscalComponent} from './detalhes.notafiscal';
const routes: Routes = [
    {path: '', component: NotafiscalComponent},
    {path: 'perfil/:id', component: DetalhesNotaFiscalComponent,
        canActivate: [AuthGuard]},
    {path: ':tipoentidade/:identidade/dados/:id', component: DetalhesNotaFiscalComponent,
        canActivate: [AuthGuard]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotafiscalRoutingModule { }
