import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';

import { NotafiscalRoutingModule } from './notafiscal-routing.module';
import {NotafiscalComponent} from "./notafiscal.component";
import {PageHeaderModule} from "../shared/modules/page-header/page-header.module";
import {DataTableModule} from "angular2-datatable";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {NgxDatatableModule} from "@swimlane/ngx-datatable";
import {TextMaskModule} from "angular2-text-mask";
import {EstaQuitadaPipe} from "../pipes/esta-quitada.pipe";
import {DetalhesNotaFiscalComponent} from "./detalhes.notafiscal";
import {NgxMaskModule} from 'ngx-mask';
import {CurrencyMaskModule} from 'ng2-currency-mask';

@NgModule({
  imports: [
    CommonModule,
    NotafiscalRoutingModule,
      PageHeaderModule,
      DataTableModule,
      FormsModule,
      HttpModule,
      NgxDatatableModule,
      TextMaskModule,
      NgxMaskModule,
      CurrencyMaskModule,
      ReactiveFormsModule,
  ],
  declarations: [NotafiscalComponent, EstaQuitadaPipe, DetalhesNotaFiscalComponent],
    exports: [DetalhesNotaFiscalComponent, NotafiscalComponent],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]

})
export class NotafiscalModule { }
