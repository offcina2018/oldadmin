import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import { AppSettings } from './app.settings';
@Injectable()
export class AuthService {
    isLoggedin = false;
    http: Http;
    authToken = '';
    // nav: NavController;
    static get parameters() {
        return [[Http]
            // , [NavController]
         ];
    }

    constructor(http: Http) {
        this.http = http;
        // this.nav = navcontroller;
    }

    login(user) {
        const headers = new Headers();
        const creds = 'email=' + user.email +
            '&password=' + user.password;
        headers.append('Content-Type', 'application/x-www-form-urlencoded');

        return new Promise(resolve => {

            this.http.post(AppSettings.API_ENDPOINT + '/auth/signin', creds, {headers: headers}).subscribe(retorno => {
                this.isLoggedin = false;
                if (retorno.json().data.token) {
                    const x = retorno.json().data.token;
                    user = retorno.json().data.user;
                    localStorage.setItem('token', x);
                    localStorage.setItem('user', JSON.stringify(user));
                    this.isLoggedin = true;
                }
                resolve(this.isLoggedin);

            });

        });


    }
    register(user) {

        return new Promise(resolve => {
            const creds = 'username=' + user.username +
                '&email=' + user.email +
                '&password=' + user.password;

            const headers = new Headers();
            headers.append('Content-Type', 'application/x-www-form-urlencoded');
            this.http.post(AppSettings.API_ENDPOINT + '/auth/signup', creds, {headers: headers}).subscribe(retorno => {
                if (retorno.json().data.token) {
                    console.log('HABEMUS TOKEN');
                    // this.localStorageService.set('someObject', object);
                    const x = retorno.json().data.token;
                    localStorage.setItem('token', x);

                    user = retorno.json().data.user;
                    localStorage.setItem('user', JSON.stringify(user));
                    resolve(true);
                }else {
                    // 2DO - Tratar erro
                    console.log(retorno.json());
                    resolve(false);
                }
            });
        });

    }
    checkLogin() {
        const token = localStorage.getItem('token');
        if (token.length > 0) {
            return true;
        }else {
            return false;
        }
    }
    getinfo() {
        return new Promise(resolve => {
            const headers = new Headers();
            headers.append('Authorization', 'Bearer ' + this.authToken);
            this.http.get( AppSettings.API_ENDPOINT + '/getinfo', {headers: headers}).subscribe(data => {
                resolve(data);

            });
        });
    }
        logout() {
        this.isLoggedin = false;
        // window.localStorage.clear();
    }

}
