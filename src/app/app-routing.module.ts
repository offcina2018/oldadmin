import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './shared';

const routes: Routes = [
    {
        path: '',
        loadChildren: './layout/layout.module#LayoutModule',
        canActivate: [AuthGuard]
    },
    { path: 'login', loadChildren: './login/login.module#LoginModule' },
    { path: 'signup', loadChildren: './signup/signup.module#SignupModule' },
    { path: 'not-found', loadChildren: './not-found/not-found.module#NotFoundModule' },
    { path: 'perfil', loadChildren: './associado/associado.module#AssociadoModule' },
    { path: 'associado', loadChildren: './associado/associado.module#AssociadoModule' },
    { path: 'empresas', loadChildren: './empresa/empresa.module#EmpresaModule' },
    { path: 'enderecos', loadChildren: './endereco/endereco.module#EnderecoModule' },
    { path: 'areaatuacao', loadChildren: './areaatuacao/areaatuacao.module#AreaatuacaoModule' },
    { path: 'notasfiscais', loadChildren: './notafiscal/notafiscal.module#NotafiscalModule' },
    { path: 'infobancaria', loadChildren: './infobancaria/infobancaria.module#InfobancariaModule' },
    { path: 'pagamentos', loadChildren: './pagamento/pagamento.module#PagamentoModule' },
    { path: '**', redirectTo: 'not-found' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
