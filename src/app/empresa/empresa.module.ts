import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmpresaComponent} from './empresa.component';

import { PageHeaderModule } from '../shared';

import {DataTableModule} from "angular2-datatable";
import {FormsModule} from "@angular/forms";

import { DetalhesEmpresaComponent} from './detalhes.empresa.component';
import {  ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {DataFilterPipe} from "../datafilter.pipe";

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import {CpfPipe} from '../cpf.pipe' ;
import { TextMaskModule } from 'angular2-text-mask';
import {EmpresaRoutingModule} from "./empresa-routing.module";
import {EmpresaDadosBrasilComponent} from "./empresa.dadosbrasil.component";
import {StatusEmpresaPipe} from "../pipes/status-empresa.pipe";
import {NgxMaskModule} from "ngx-mask";
@NgModule({
    imports: [
        CommonModule,
        // BrowserModule,
        EmpresaRoutingModule,
        PageHeaderModule,
        DataTableModule,
        FormsModule,
        HttpModule,
        NgxDatatableModule,
        TextMaskModule,
        NgxMaskModule,
        ReactiveFormsModule,
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    ,
    declarations: [
        EmpresaComponent, DetalhesEmpresaComponent, EmpresaDadosBrasilComponent, StatusEmpresaPipe
    ],
    exports: [
        EmpresaComponent, DetalhesEmpresaComponent, EmpresaDadosBrasilComponent
    ]
})
export class EmpresaModule { }
