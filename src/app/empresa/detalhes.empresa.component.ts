import { RequestOptions, Headers, Http } from '@angular/http';
import { AppSettings } from '../app.settings';
// import {Router} from "@angular/router";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ApiLiame } from '../services/apiliame.service';
import { AssociadoModel } from '../services/models/associado';
import {Page} from '../services/models/page';
import {ViewChild, OnInit, ViewEncapsulation, Component, Input} from '@angular/core';
import {CpfPipe} from '../cpf.pipe';
import {Form, FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import {EmpresaModel} from "../services/models/empresa";
import {NotificationService} from "ng2-notify-popup";

/**
 * Created by offcina on 28/06/17.
 */
@Component({
    selector: 'app-detalhes.empresa',
    templateUrl: './detalhes.empresa.component.html',
    styleUrls: ['./empresa.component.scss'],
    providers: [ApiLiame,  CpfPipe, NotificationService ],
    encapsulation: ViewEncapsulation.None
})
export class DetalhesEmpresaComponent implements OnInit {

    @ViewChild('myTable') table: any;
    @Input() ass: Observable<EmpresaModel>;
    loading = false;
    token: string;
    public celular = ['(', /[1-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
    temp = [];
    rota: Router;

    public alerta: any = [];
    url: string;
    handleError: any;
     row = [];
     idEmpresa: any ;
     telFax:any;
    empresas: EmpresaModel ;
    errorMessage: any;
    page = new Page();
    rows: Array<EmpresaModel> = [];
    meuForm2: any;
    fmBuilder: FormBuilder;
    constructor(private route: ActivatedRoute,
                private router: Router,
                public api: ApiLiame ,
                public api2: Http,
    public notificationService :  NotificationService,

    fb: FormBuilder) {

        this.meuForm2 = new FormGroup({
            // email: new FormControl('',Validators.required),
            // Estado: new FormControl('',Validators.required),
            // Usuario: new FormControl('',Validators.required),
            // : new FormControl('',Validators.required),
            // inscricao_municipal: new FormControl('',Validators.required),
            nome_fantasia: new FormControl('',Validators.required),
            status: new FormControl('',Validators.required),
            // cnpj_cpf: new FormControl('',Validators.required),
            razao_social: new FormControl('',Validators.required),
            // inscricao_estadual: new FormControl('',Validators.required),
            // telefone: new FormControl('',Validators.required),
            // telefone_fax: new FormControl('',Validators.required),
            // endereco: new FormControl('',Validators.required),
            // endereco_cidade: new FormControl('',Validators.required),
            // endereco_bairro: new FormControl('',Validators.required),
            // endereco_numero: new FormControl('',Validators.required),
            // endereco_cep: new FormControl('',Validators.required),
            // endereco_complemento: new FormControl('',Validators.required),
            // auditor: new FormControl('',Validators.required),
            obs: new FormControl('',Validators.required),
            cod_pais: new FormControl('',Validators.required),
            updatedAt: new FormControl('',Validators.required),
            createdAt: new FormControl('',Validators.required),
          //  auditor : new FormControl('',Validators.required),
            id: new FormControl('',Validators.required)



        });


        this.route.params
            .switchMap((params: Params) => this.api.getEmpresa(params['id']))
            // .subscribe((ret: AssociadoModel) => this.associado = ret)
            .subscribe((ret: any) => {
                (<FormGroup>this.meuForm2).setValue(ret, {onlySelf: true});
                this.idEmpresa = ret.id;


            });

    };



    ngOnInit() {


    };

    salvarEmpresa() {
        console.log('salva');
        this.token = AppSettings.apitoken;
        this.url = AppSettings.API_ENDPOINT;
        this.alerta.avisoSucesso = '';
        this.meuForm2.removeControl('createdAt');
        this.meuForm2.removeControl('updatedAt');

//        console.log(this.api.saveEmpresa(this.idEmpresa));
        this.api.saveEmpresa(this.idEmpresa, this.meuForm2.value).do(x => {
            this.notificationService.show('Salvo', {
                position: 'bottom',
                duration: '2000',
                type: 'error' });
            // this.setPage(1);
            // this.router.navigate(['/empresas']);
        }, err => {
            this.notificationService.show('Erro', {
                position: 'bottom',
                duration: '2000',
                type: 'error' });

        })

            .catch(this.handleError)
            .subscribe((ok) => {
                console.log(ok);
            });
        this.router.navigate(['/empresas']);


    }






    deletaEmpresa(id) {
    //    console.log(idEmpresa);
      //  const id = linha.idEmpresa;
        console.log('deletaEmpresa');
        this.token = AppSettings.apitoken;
        this.url = AppSettings.API_ENDPOINT;
        this.alerta.avisoSucesso = '';


        console.log(this.api.delEmpresa(id));

        this.api.delEmpresa(id).do(x => {
            this.notificationService.show('Erro', {
                position: 'bottom',
                duration: '2000',
                type: 'error' });
                // this.setPage(1);
                // this.router.navigate(['/empresas']);

            },
        err => {
            this.notificationService.show('Erro', {
            position: 'bottom',
            duration: '2000',
            type: 'error' });
    })
            .catch(this.handleError)
            .subscribe((ok) => {
                console.log(ok);
            });
        //this.router.navigate(['/empresas']);


    }
    voltaEmp() {
        this.router.navigate(['/empresas']);
    }
    loga(event){
        console.log(event);
    }
}
