import { Component, OnInit } from '@angular/core';
import { ApiLiame } from '../services/apiliame.service';
import { Observable } from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';
// import { DataTableModule } from 'angular2-datatable';
import { DataFilterPipe } from '../datafilter.pipe';
import {Http, RequestOptions} from '@angular/http';
import {  ViewEncapsulation, ViewChild } from '@angular/core';
import {PagedData} from "../services/models/paged-data" ;
// import {CorporateEmployee} from './model/corporate-employee';
import {AppSettings} from '../app.settings';
import { CpfPipe} from '../cpf.pipe';
import {DetalhesEmpresaComponent} from './detalhes.empresa.component'
import { AuthService } from '../auth.service';
import {Router} from '@angular/router';
import {id} from "@swimlane/ngx-datatable/release/utils";
import {Page} from "../services/models/page";
import {EmpresaModel} from "../services/models/empresa";
import {StatusEmpresaPipe} from "../pipes/status-empresa.pipe";

@Component({
    selector: 'app-empresa',
    templateUrl: './empresa.component.html',
    styleUrls: ['./empresa.component.scss'],
    providers: [ApiLiame, CpfPipe],
    encapsulation: ViewEncapsulation.None
})
export class EmpresaComponent implements OnInit {
    @ViewChild('myTable') table: any;
    loading = false;
    token: string;
    public celular = ['(', /[1-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
    temp = [];
    rota: Router;
    public alerta: any = [];
    url: string;
    handleError: any;
    empresas: Array<EmpresaModel> = [];
    errorMessage: any;
    page = new Page();
    busca = '';
    autenticador: AuthService;
    rows: Array<EmpresaModel> = [];

    constructor(
                public api: ApiLiame,
                public api2: Http,
                public auth: AuthService,
                public router: Router ) {
        /* seta variaveis iniciais */
        this.page.pageNumber = 0;
        this.page.camposBusca = ['nome_fantasia', 'razao_social' ];
        this.page.auxiliares = ['notas', 'dados', 'area_atuacao' , 'contato' , 'endereco' ];
        this.rota = router;
        this.autenticador = auth;
        this.page.size = 20;
    };
    ngOnInit() {
        if (!this.autenticador.checkLogin()) {
            this.rota.navigate(['/dashboard']);
        }
        this.setPage({ offset: 0 });
    };

    /**
     * Navega para detalhes
     * @param id id do associado
     */
    detalhes() {
        /* navega para detalher */
        this.router.navigate(['/empresas/perfil', 0]);
    }
    detalhe(id: number) {
        /* navega para detalher */
        this.router.navigate(['/empresas/perfil', id]);
    }

    /**
     * Filtrar por campo de busca
     */
    filtrar() {

        this.page.valorBusca =  this.busca;
        console.log('updateFilter');
        console.log(event);
        this.api.getEmpresas(this.page).subscribe(
            pagedData =>  this.rows = pagedData
        );
    }

    /**
     * ORDENACAO
     * @param event evento
     */
    onSort(event) {

        this.page.tipoOrdenacao = event.newValue;
        this.page.campoOrdenacao = event.sorts[0].prop;
        console.log(this.page);
        this.page.pageNumber = 0;
        this.page.camposBusca = ['nome_fantasia', 'razao_social', 'id' ];
        this.page.auxiliares = ['dados', 'contato', 'nfs'];

        this.loading = true;
        this.api.getEmpresas(this.page).subscribe(
            // pagedData =>  this.rows = pagedData,
            pagedData =>  this.rows = pagedData,
        );
        this.empresas = this.rows;

        this.loading = false;
    }

    /* cadastrar dados para empresa */
    cadastrarEnderecoEmpresa(idempresa, id){
        this.router.navigate(['/enderecos/empresas/' + idempresa + '/endereco', id ]);
    }
    /* cadastrar dados para empresa */
    cadastrarDadosEmpresa(idempresa , id) {
        console.log('/empresas/' + idempresa + '/dados');
        this.router.navigate(['/empresas/' + idempresa + '/dados', id ]);
    }
    //:id/dados/:iddados
    /**
     * Paginacao
     * @param page Pagina
     */
    setPage(pageInfo) {
        console.log(pageInfo);
        this.page.pageNumber = pageInfo.offset;
        this.api.getEmpresas(this.page).subscribe(
            pagedData =>  this.rows = pagedData,
            // pagedData =>  this.associados = pagedData,
        );
        this.empresas = this.rows;
    }

    /**
     * Abre Linha
     * @param row numero da linha
     */
    toggleExpandRow(row) {
        console.log('Toggled Expand Row!', row);
        this.table.rowDetail.toggleExpandRow(row);
    }

    onDetailToggle(event) {
        console.log('Detail Toggled', event);
    }

}
