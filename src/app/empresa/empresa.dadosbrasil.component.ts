/**
 * Created by paulomarinho on 27/08/17.
 */
import { RequestOptions, Headers, Http } from '@angular/http';
import { AppSettings } from '../app.settings';
// import {Router} from "@angular/router";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ApiLiame } from '../services/apiliame.service';
import { AssociadoModel } from '../services/models/associado';
import {Page} from '../services/models/page';
import {ViewChild, OnInit, ViewEncapsulation, Component, Input} from '@angular/core';
import {CpfPipe} from '../cpf.pipe';
import {Form, FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import {EmpresaModel} from "../services/models/empresa";
import {NotificationService} from "ng2-notify-popup";
import {EmpresaService} from "../services/empresa.service";
import {DadosbrasilModel} from "../services/models/dadosbrasil";

/**
 * Created by offcina on 28/06/17.
 */
@Component({
    selector: 'app-dadosbrasil.empresa',
    templateUrl: './empresa.dadosbrasil.component.html',
    styleUrls: ['./empresa.component.scss'],
    providers: [ApiLiame,  CpfPipe, NotificationService,EmpresaService ],
    encapsulation: ViewEncapsulation.None
})
export class EmpresaDadosBrasilComponent implements OnInit {

    @ViewChild('myTable') table: any;
    @Input() ass: Observable<EmpresaModel>;
    loading = false;
    token: string;
    public celular = ['(', /[1-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
    temp = [];
    rota: Router;

    public alerta: any = [];
    url: string;
    handleError: any;
    row = [];
    idEmpresa: any ;
    telFax:any;
    empresas: EmpresaModel ;
    dados: DadosbrasilModel;
    idDados: number = 0;
    errorMessage: any;
    page = new Page();
    rows: Array<EmpresaModel> = [];
    meuForm2: any;
    fmBuilder: FormBuilder;
    constructor(private route: ActivatedRoute,
                private router: Router,
                public api: ApiLiame ,
                public api2: Http,
                public apiempresa: EmpresaService,
                public notificationService :  NotificationService,

                fb: FormBuilder) {

        this.meuForm2 = new FormGroup({
            inscricao_municipal: new FormControl('',Validators.required),
            cnpj_cpf: new FormControl('',Validators.required),
            inscricao_estadual: new FormControl('',Validators.required),
            updatedAt: new FormControl('',Validators.required),
            createdAt: new FormControl('',Validators.required),
            id: new FormControl('',Validators.required),
            empresa: new FormControl('')



        });

        this.route.params
            .subscribe(params => {
                this.idEmpresa =  params['id'];
                this.idDados =  params['iddados'];
                if (this.idDados > 0 ) {
                    this.apiempresa.getDados(this.idDados)
                        .subscribe((ret: any) => {
                            (<FormGroup>this.meuForm2).setValue(ret, {onlySelf: true});
                            this.idDados = ret.id;
                        });
                }
            });


    };



    ngOnInit() {


    };

    salvaDados(){
        console.log('salva');
        console.log(this.idEmpresa);
        this.meuForm2.controls['empresa'].setValue(this.idEmpresa);

        this.token = AppSettings.apitoken;
        this.url = AppSettings.API_ENDPOINT;
        this.alerta.avisoSucesso = '';
        this.meuForm2.removeControl('createdAt');
        this.meuForm2.removeControl('updatedAt');
console.log('iddados');
console.log(this.idDados);
//        console.log(this.api.saveEmpresa(this.idEmpresa));
        this.apiempresa.saveDados(this.idDados, this.meuForm2.value).do(x => {
            this.notificationService.show('Salvo', {
                position: 'bottom',
                duration: '2000',
                type: 'error' });
            this.idDados = x.id;
            // this.setPage(1);
            // this.router.navigate(['/empresas']);
        }, err => {
            this.notificationService.show('Erro', {
                position: 'bottom',
                duration: '2000',
                type: 'error' });

        })

            .catch(this.handleError)
            .subscribe((ok) => {
                console.log(ok);
            });
        //  this.router.navigate(['/empresas']);


    }






    deletaDados() {
        //    console.log(idEmpresa);
        //  const id = linha.idEmpresa;
        console.log('deletaEmpresa');
        this.token = AppSettings.apitoken;
        this.url = AppSettings.API_ENDPOINT;
        this.alerta.avisoSucesso = '';


        //console.log(this.apiempresa.deletaDados(id, this.meuForm2.value()));
        this.apiempresa.deletaDados(this.idDados, this.meuForm2.value).do(x => {
                this.notificationService.show('Suuucesso', {
                    position: 'bottom',
                    duration: '2000',
                    type: 'success' });
                // this.setPage(1);
                // this.router.navigate(['/empresas']);

            },
            err => {
                this.notificationService.show('Erro', {
                    position: 'bottom',
                    duration: '2000',
                    type: 'error' });
            })
            .catch(this.handleError)
            .subscribe((ok) => {
                console.log(ok);
            });
        //this.router.navigate(['/empresas']);


    }
    voltaEmp() {
        this.router.navigate(['/empresas']);
    }
    loga(event){
        console.log(event);
    }
}
