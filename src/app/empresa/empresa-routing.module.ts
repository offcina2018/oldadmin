import {NgModule} from  '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { AuthGuard } from '../shared';
import { EmpresaComponent} from "./empresa.component";
import {DetalhesEmpresaComponent} from "./detalhes.empresa.component";
import { EmpresaDadosBrasilComponent} from './empresa.dadosbrasil.component';

const routes: Routes = [

    {path: '', component: EmpresaComponent,
        canActivate: [AuthGuard]},
    {path: 'perfil/:id', component: DetalhesEmpresaComponent,
        canActivate: [AuthGuard]},
    {path: ':id/dados/:iddados', component: EmpresaDadosBrasilComponent,
        canActivate: [AuthGuard]}

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class EmpresaRoutingModule {
}
