import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import { AreaatuacaoRoutingModule } from './areaatuacao-routing.module';
import { PageHeaderModule } from '../shared';
import { DataTableModule } from 'angular2-datatable';
import { FormsModule } from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
//  import {AssociadoDetalhesComponent} from './associado.component';
import {  ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import {CpfPipe} from '../cpf.pipe' ;
import { TextMaskModule } from 'angular2-text-mask';
import {DetalhesareaatucacaoComponent} from './detalhes.areaatuacao.component';
import { AssociadoModel } from '../services/models/associado';
import {AreaatuacaoComponent} from './areaatuacao.component';

@NgModule({
    imports: [
        CommonModule,
        // BrowserModule,
        AreaatuacaoRoutingModule,
        PageHeaderModule,
        DataTableModule,
        FormsModule,
        HttpModule,
        NgxDatatableModule,
        TextMaskModule,
        ReactiveFormsModule
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    ,
    declarations: [
        AreaatuacaoComponent, DetalhesareaatucacaoComponent
    ],
    exports: [
        AreaatuacaoComponent, DetalhesareaatucacaoComponent
    ],
})
export class AreaatuacaoModule { }
