import { Component, OnInit } from '@angular/core';
import { ApiLiame } from '../services/apiliame.service';
import { Observable } from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';
import {Http, RequestOptions} from '@angular/http';
import {Headers} from '@angular/http';
import {Page} from '../services/models/page';
import {  ViewEncapsulation, ViewChild } from '@angular/core';
import {AppSettings} from '../app.settings';
import { CpfPipe} from '../cpf.pipe';
import {DetalhesareaatucacaoComponent} from './detalhes.areaatuacao.component';
import { AuthService } from '../auth.service';
import {Router} from '@angular/router';
import {id} from "@swimlane/ngx-datatable/release/utils";
import {AreaatuacaoModel} from "../services/models/areaatuacao";
import { trigger,state,style,transition,animate,keyframes } from '@angular/animations';

@Component({
  selector: 'app-areaatuacao',
  templateUrl: './areaatuacao.component.html',
  styleUrls: ['./areaatuacao.component.scss']
})
export class AreaatuacaoComponent implements OnInit {

    @ViewChild('myTable') table: any;
    loading = false;
    token: string;
    public celular = ['(', /[1-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
    temp = [];
    rota: Router;
    public alerta: any = [];
    url: string;
    handleError: any;
    associados: Array<AreaatuacaoModel> = [];
    errorMessage: any;
    page = new Page();
    busca = '';
    autenticador: AuthService;
    rows: Array<AreaatuacaoModel> = [];

    constructor(public api: ApiLiame,
                public api2: Http,
                public auth: AuthService,
                public router: Router ) {
        /* seta variaveis iniciais */
        this.page.pageNumber = 0;
        this.rota = router;
        this.page.camposBusca = ['nome' ];

        this.autenticador = auth;
        this.page.size = 20;
    };
    ngOnInit() {
        if (!this.autenticador.checkLogin()) {
            this.rota.navigate(['/dashboard']);
        }
        this.setPage({ offset: 0 });
    };

    /**
     * Navega para detalhes
     * @param id id do associado
     */
    detalhes(id) {
        /* navega para detalher */
        this.router.navigate(['/areaatuacao/dados',id]);
    }

    /**
     * Novo cadastro
     */
    irParaCadastro() {
        /* navega para detalher */
        this.router.navigate(['/areaatuacao/dados',0]);
    }
    /**
     * Filtrar por campo de busca
     */
    filtrar() {

        this.page.valorBusca =  this.busca;
        console.log('updateFilter');
        console.log(event);
        this.api.listaAreaatuacao(this.page).subscribe(
            pagedData =>  this.rows = pagedData,
        );
    }

    /**
     * ORDENACAO
     * @param event evento
     */
    onSort(event) {

        this.page.tipoOrdenacao = event.newValue;
        this.page.campoOrdenacao = event.sorts[0].prop ;
        console.log(this.page);
        this.page.pageNumber = 0;
        this.loading = true;
        this.api.listaAreaatuacao(this.page).subscribe(
            // pagedData =>  this.rows = pagedData,
            pagedData =>  this.rows = pagedData,
        );
        this.associados = this.rows;

        this.loading = false;
    }



    /**
     * Paginacao
     * @param page Pagina
     */
    setPage(pageInfo) {
        console.log(pageInfo);
        this.page.pageNumber = pageInfo.offset;
        this.api.listaAreaatuacao(this.page).subscribe(
            pagedData =>  this.rows = pagedData,
            // pagedData =>  this.associados = pagedData,
        );
        this.associados = this.rows;
    }

    /**
     * Abre Linha
     * @param row numero da linha
     */
    toggleExpandRow(row) {
        console.log('Toggled Expand Row!', row);
        this.table.rowDetail.toggleExpandRow(row);
    }

    onDetailToggle(event) {
        console.log('Detail Toggled', event);
    }

}
