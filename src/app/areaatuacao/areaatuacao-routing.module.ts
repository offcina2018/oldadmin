import {NgModule} from  '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { AuthGuard } from '../shared';
import { AreaatuacaoComponent} from "./areaatuacao.component";
import {DetalhesareaatucacaoComponent} from "./detalhes.areaatuacao.component";


const routes: Routes = [

    {path: '', component: AreaatuacaoComponent,
        canActivate: [AuthGuard]},
    {path: 'dados/:id', component: DetalhesareaatucacaoComponent,
        canActivate: [AuthGuard]},
    {path: ':tipoentidade/:identidade/areaatuacao/:id', component: DetalhesareaatucacaoComponent,
        canActivate: [AuthGuard]},


];

@NgModule({

    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]

})
export class AreaatuacaoRoutingModule {
}
