import {Component, OnInit, ViewChild,ViewEncapsulation} from '@angular/core';
import {Page} from "../services/models/page";
import {Router} from "@angular/router";
import {AuthService} from "../auth.service";
import {ApiLiame} from "../services/apiliame.service";
import {Http} from "@angular/http";
import {NotafiscalService} from "../services/notafiscal.service";
import {PagamentoModel} from "../services/models/pagamento";
import {PagamentoService} from "../service/pagamento.service";
import * as moment from "moment";
import _date = moment.unitOfTime._date;
import {statusPagamento} from "../app.settings";
import {StatusNfPipe} from "../pipes/status-nf.pipe";
import {NotificationService} from "ng2-notify-popup";


@Component({
  selector: 'app-pagamento',
  templateUrl: './pagamento.component.html',
  styleUrls: ['./pagamento.component.scss'],
    providers: [PagamentoService, NotafiscalService, StatusNfPipe, NotificationService],
    encapsulation: ViewEncapsulation.None
})
export class PagamentoComponent implements OnInit {
    @ViewChild('myTable') table: any;
    loading = true;
    token: string;
    public celular = ['(', /[1-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
    temp = [];
    rota: Router;
    public alerta: any = [];
    url: string;
    handleError: any;
    linhas: Array<PagamentoModel> = [];
   // empresas: Array<EmpresaModel> = [];
    errorMessage: any;
    page = new Page();
    busca = '';
    pagamentos =[];
    autenticador: AuthService;
    rows: Array<PagamentoModel> = [];
    lstEmpresas: any;
    lstAssociados: any;
    //lstStatus: any;
    lstNotas: any;
    idLstAssociado: any;
    idLstEmpresa: any;
    //idLstStatus: any;
    idLstNotas: any;
    dtLstInicio: Date;
    dtLstFim: Date;
    public statusPagamento = statusPagamento;
    keys: any;
  constructor( public api: ApiLiame,
               public api2: Http,
               public api3: NotafiscalService,
               public auth: AuthService,
               public router: Router,
               public notificationService: NotificationService) {
      this.page.pageNumber = 0;
      this.page.auxiliares = [ 'associado', 'nfs'];
      this.rota = router;
      this.autenticador = auth;
      this.page.size = 20;
      this.comboEmpresas();
      this.comboAssociados();

      //this.comboStatus();
      this.comboNotas();
      this.keys = Object.keys(this.statusPagamento).filter(Number);
  }
    comboEmpresas() {
        let page = new Page();
        page.pageSize = 5000;
        page.pageNumber = 0;
        this.page.camposBusca = ['nfs'];
        page.camposRetorno = ['id', 'nome_fantasia'];
        page.campoOrdenacao = 'nome_fantasia';
        page.tipoOrdenacao = 'asc';
        this.api.getEmpresas(page).subscribe(ret =>{
            this.lstEmpresas = ret;
            // console.log('leu');
        });
    }
    changeLstEmpresas(x) {
        if (x.value == 0) {
            delete       this.page.fixo.empresa;
        }else{
            this.page.fixo.empresa = x.value;
        }
        this.setPage({ offset: 0 });
        // console.log(x.value);
    }

    /**
     * Filtrar por campo de busca
     */
    filtrar() {

        this.page.valorBusca =  this.busca;
        this.api.listaPagamentos(this.page).subscribe(
            pagedData => {
                this.linhas = pagedData,
                    this.pagamentos = this.linhas;
            },
                erro => {
                        this.notificationService.show(erro.toString(), {
                            position: 'bottom',
                            duration: '2000',
                            type: 'error' });
                    }
        );
    }
    /*comboStatus() {
        let page = new Page();
        page.pageSize = 5000;
        page.pageNumber = 0;
        page.camposRetorno = ['id', 'nome_fantasia'];
        page.campoOrdenacao = 'nome_fantasia';
        page.tipoOrdenacao = 'asc';
        this.api.getStatus(page).subscribe(ret =>{
            this.lstStatus = ret;
            console.log('leu');
        });
    }
    changeLstStatus(x) {
        if (x.value == 0) {
            delete       this.page.fixo.status;
        }else{
            this.page.fixo.status = x.value;
        }
        this.setPage({ offset: 1 });
        console.log(x.value);
    }*/
    comboNotas() {
        let page = new Page();
        page.pageSize = 5000;
        page.pageNumber = 0;
        page.camposRetorno = ['id', 'numero'];
        page.campoOrdenacao = 'numero';
        page.tipoOrdenacao = 'asc';
        this.api3.listaNotas(page).subscribe(ret =>{
            this.lstNotas = ret;
            // console.log('leuppppppppp');
        });
    }
    changeLstNotas(x) {
        if (x.value == 0) {
            delete       this.page.fixo.nfs;
        }else {
            this.page.fixo.nfs = x.value;
        }
        this.setPage({ offset: 0 });
        // console.log(x.value);
    }
    comboAssociados() {
        const page = new Page();
        page.pageSize = 5000;
        page.pageNumber = 0;
        page.camposRetorno = ['id', 'nome'];
        page.campoOrdenacao = 'nome';
        page.tipoOrdenacao = 'asc';
        this.api.getAssociados(page).subscribe(ret =>{
            this.lstAssociados = ret;
            // console.log('leu');
        });
    }
    changeLstAssociados(x) {
        if (x.value == 0) {
            delete this.page.fixo.associado;
        }else {
            this.page.fixo.associado = x.value;
        }

        this.setPage({ offset: 0 });
        // console.log(x.value);
    }
    changeLstStatus(x) {
        if (x.value == 0) {
            delete this.page.fixo.status;
        }else {
            this.page.fixo.status = x.value;
        }

        this.setPage({ offset: 0 });
        // console.log(x.value);
    }
    changeDataInicio(x) {
      // http://liame.org.br:3000/v1/associados?where={"data_nascimento":{">":"1978-01-01"}}&fields=data_nascimento
        // FINAL
        // http://liame.org.br:3000/v1/associados?where={"data_nascimento":{">":"1978-01-01","<":"1987-01-01"}}&fields=data_nascimento
      this.page.filtroData.campo = 'data_quitacao';
      this.page.filtroData.inicio = x.value;
      this.setPage({ offset: 0 });

        // this.page.fixo.datainicio = x.value;
    }
    changeDataFim(x) {
      // http://liame.org.br:3000/v1/associados?where={"data_nascimento":{">":"1978-01-01"}}&fields=data_nascimento
        // FINAL
        // http://liame.org.br:3000/v1/associados?where={"data_nascimento":{">":"1978-01-01","<":"1987-01-01"}}&fields=data_nascimento
      this.page.filtroData.campo = 'data_quitacao';
      this.page.filtroData.fim = x.value;
      this.setPage({ offset: 0 });

        // this.page.fixo.datainicio = x.value;
    }
    ngOnInit() {
      if (!this.autenticador.checkLogin()) {
          this.rota.navigate(['/dashboard']);
      }
      this.setPage({ offset: 0 });
    }
    /**
     * Navega para detalhes
     * @param id id do associado
     */
    detalhes(id) {
        /* navega para detalher */
        this.router.navigate(['/pagamentos/perfil', id]);
    }
    onSort(event) {

     this.page.tipoOrdenacao = event.newValue;
     this.page.campoOrdenacao = event.sorts[0].prop;
     // console.log(this.page);
     this.page.pageNumber = 0;
      this.loading = true;
     this.api.listaPagamentos(this.page).subscribe(
        // pagedData =>  this.rows = pagedData,
        pagedData =>  this.linhas = pagedData,
     );
     this.pagamentos = this.linhas;

    }



    /**
     * Paginacao
     * @param page Pagina
     */
    setPage(pageInfo) {
        // console.log(pageInfo);
        this.page.pageNumber = pageInfo.offset;
        this.api.listaPagamentos(this.page).subscribe(
            pagedData =>  {
                this.linhas = pagedData;
                this.loading = false;
            }
            // pagedData =>  this.associados = pagedData,
        );
        this.pagamentos = this.linhas;
    }

    /**
     * Abre Linha
     * @param row numero da linha
     */
    toggleExpandRow(linha) {
        console.log('Toggled Expand Row!', linha);
        this.table.rowDetail.toggleExpandRow(linha);
    }

    onDetailToggle(event) {
        console.log('Detail Toggled', event);
    }
}
