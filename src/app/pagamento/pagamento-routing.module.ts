import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PagamentoComponent} from './pagamento.component';
import {DetalhesPagamentoComponent} from './detalhes.pagamento';
import {AuthGuard} from "../shared/guard/auth.guard";
const routes: Routes = [
    {path: '', component: PagamentoComponent},
    {path: 'perfil/:id', component: DetalhesPagamentoComponent,
        canActivate: [AuthGuard]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagamentoRoutingModule { }
