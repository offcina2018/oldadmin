import { RequestOptions, Headers, Http } from '@angular/http';
import {AppSettings, statusPagamento} from '../app.settings';
// import {Router} from "@angular/router";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ApiLiame } from '../services/apiliame.service';
import { EnderecoModel } from '../services/models/endereco';
import {Page} from '../services/models/page';
import {ViewChild, OnInit, ViewEncapsulation, Component, Input} from '@angular/core';
import {Form, FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';
import {NotificationService} from "ng2-notify-popup";
import {NotafiscalModel} from "../services/models/notafiscal";
import {NotafiscalService} from "../services/notafiscal.service";
import {PagamentoService} from "../services/pagamento.service";
import { StatusPagPipe} from '../pipes/status-pag.pipe';
/**
 * Created by offcina on 28/06/17.
 */
@Component({
    selector: 'app-detalhespagamento',
    templateUrl: './detalhes.pagamento.html',
    styleUrls: ['./pagamento.component.scss'],
    providers: [ApiLiame, NotafiscalModel, DatePipe,
        NotificationService , NotafiscalService, PagamentoService, StatusPagPipe],
    encapsulation: ViewEncapsulation.None
})
export class DetalhesPagamentoComponent implements OnInit {

    /* propriedades */
    @ViewChild('myTable') table: any;
    @Input() ass: Observable<EnderecoModel>;
    loading = false;
    token: string;
    public celular = ['(', /[1-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
    temp = [];
    rota: Router;
    public alerta: any = [];
    coordenadores: any;
    url: string;
    handleError: any;
    // rows = [];
    idPagamento: any;
    idEntidade: any;
    tipoEntidade: any;
    errorMessage: any;
    page = new Page();
    rows: Array<NotafiscalModel> = [];
    meuForm: any;
    fmBuilder: FormBuilder;
    associado: any;
    contas: any;
    public historico: any = [];
    public pagamento: any = {};
    public statusPagamento = statusPagamento;
    keys: any;
    constructor(private route: ActivatedRoute,
                private router: Router,
                public api: ApiLiame ,
                public apipag: PagamentoService,
                public api2: Http,
                private fb: FormBuilder,
                private notafiscal: NotafiscalModel,
                public notificationService: NotificationService) {

        // var statusOptions = Object.keys(this.statusPagamento);
        // this.statusOptions = statusOptions.slice(statusOptions.length / 2);
        /* setup form */
        this.keys = Object.keys(this.statusPagamento).filter(Number);

        this.meuForm = new FormGroup({
            // descricao: new FormControl ('', Validators.required),
            valor: new FormControl ('', Validators.required),
            status: new FormControl ('', Validators.required),
            obs : new FormControl('', Validators.required),
            taxa_adm: new FormControl ('', Validators.required),
            desconto: new FormControl ('', Validators.required),
            acrescimo: new FormControl ('', Validators.required),
            data_pagamento: new FormControl ('', Validators.required),
            data_quitacao: new FormControl ('', Validators.required),
            idpagamento: new FormControl ('', Validators.required),

            contas: new FormControl ('', Validators.required),
            associado : new FormControl('',Validators.required),
            nfs : new FormControl('',Validators.required),
            id : new FormControl('',Validators.required),
            // auditor : new FormControl('',Validators.required),
            createdAt : new FormControl('',Validators.required),
            updatedAt : new FormControl('',Validators.required),
            empresa : new FormControl('',Validators.required),


        });
        this.route.params
            .subscribe(params => {
                this.idPagamento =  params['id'];
                this.idEntidade =  params['identidade'];
                this.tipoEntidade =  params['tipoentidade'];

                console.log(' sou do time ' + this.idPagamento);

                if (this.idPagamento > 0 ) {
                    this.apipag.getDados(this.idPagamento)
                        .subscribe((ret: any) => {
                            console.log('ret');
                            console.log(ret);
                            (<FormGroup>this.meuForm).setValue(ret, {onlySelf: true});
                            this.idPagamento = ret.id;
                            this.pagamento = ret ;
                            console.log('auditor');
                            console.log(this.pagamento.auditor);
                            this.associado = ret.associado ;
                            this.contas = ret.contas ;

                        });
                }
                this.api.getHistorico(this.idPagamento, 'pagamento').subscribe(ret => {
                    this.historico = ret;
                    console.log('hist');
                    console.log(this.historico);
                    console.log(ret);
                });
            });
    }

    ngOnInit() {


    };
    salvaPagamento() {
        /* salva endereco pela entidade relacionada */
        this.alerta.avisoSucesso = '';
        //this.meuForm.control()
        this.meuForm.removeControl('createdAt');
        this.meuForm.removeControl('updatedAt');
        this.meuForm.removeControl('empresa');
        this.meuForm.removeControl('id');
        this.meuForm.removeControl('nfs');
        this.meuForm.removeControl('idpagamento');
        this.meuForm.removeControl('contas');
        this.meuForm.removeControl('associado');

        this.apipag.saveDados(this.idPagamento, this.meuForm.value).subscribe(x => {
            this.notificationService.show('Salvo', {
                position: 'bottom',
                duration: '2000',
                type: 'success' });
            // this.router.navigate(['/empresas']);
        });
    }
    deletaPagamento(id) {
        /* deleta endereco pela entidade relacionada */
        console.log('deleta ende');
        this.alerta.avisoSucesso = '';

        this.apipag.deletaDados(id).do(x => {
                this.notificationService.show('Salvo', {
                    position: 'bottom',
                    duration: '2000',
                    type: 'success' })
                // this.setPage(1);
                // this.router.navigate(['/empresas']);

            },
            erro => {
                this.notificationService.show('Erro', {
                    position: 'bottom',
                    duration: '2000',
                    type: 'success' });
            })
            .catch(this.handleError)
            .subscribe((ok) => {
                console.log(ok);
            });



    }
    voltaTipo(){
        this.router.navigate(['/pagamentos']);
    }
    loga(event){
        console.log(event);
    }
}

