import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';

import {PageHeaderModule} from '../shared/modules/page-header/page-header.module';
import {DataTableModule} from 'angular2-datatable';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {TextMaskModule} from 'angular2-text-mask';
import {PagamentoComponent} from './pagamento.component';
import {PagamentoRoutingModule} from './pagamento-routing.module';
import {DetalhesPagamentoComponent} from "./detalhes.pagamento";
import {NgxMaskModule} from 'ngx-mask';
import {CurrencyMaskModule} from 'ng2-currency-mask';

@NgModule({
    imports: [
        CommonModule,
        PageHeaderModule,
        PagamentoRoutingModule,
        DataTableModule,
        FormsModule,
        HttpModule,
        NgxDatatableModule,
        TextMaskModule,
        NgxMaskModule,
        CurrencyMaskModule,
        ReactiveFormsModule
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ],

  declarations: [PagamentoComponent, DetalhesPagamentoComponent],
    exports: [PagamentoComponent, DetalhesPagamentoComponent]
})
export class PagamentoModule { }
