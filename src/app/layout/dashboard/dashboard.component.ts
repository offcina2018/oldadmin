import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {ApiliameService} from "../../apiliame.service";
import {EmpresaService} from "../../services/empresa.service";
import {NotafiscalService} from "../../services/notafiscal.service";
import {ApiLiame} from "../../services/apiliame.service";
import {Http} from "@angular/http";
import {AppSettings} from "../../app.settings";

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    providers: [ ApiliameService, EmpresaService, NotafiscalService]
})
export class DashboardComponent implements OnInit {
    public alerts: Array<any> = [];
    public sliders: Array<any> = [];
    public novasEmpresas: number;
    public novasNotas: number;
    public novosAssociados: number;
    constructor(public router: Router ,
    public apiEmpresas: EmpresaService,
    public apiNotas: NotafiscalService,
    public apiliame: ApiLiame,
    public http: Http) {
        this.sliders.push({
            imagePath: 'assets/images/slider1.jpg',
            label: 'First slide label',
            text: 'Nulla vitae elit libero, a pharetra augue mollis interdum.'
        }, {
            imagePath: 'assets/images/slider2.jpg',
            label: 'Second slide label',
            text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
        }, {
            imagePath: 'assets/images/slider3.jpg',
            label: 'Third slide label',
            text: 'Praesent commodo cursus magna, vel scelerisque nisl consectetur.'
        });
        const header = this.apiliame.montaHeader();
        this.http.get(AppSettings.API_ENDPOINT + '/associados?status=0', header).subscribe( ret => {
           const retorno = ret.json();
           this.novosAssociados = retorno.totalResultados;
               console.log(ret.json());
        });
        this.http.get(AppSettings.API_ENDPOINT + '/nota_fiscals?status=0', header).subscribe( ret => {
            const retorno = ret.json();
            this.novasNotas = retorno.totalResultados;
            console.log(ret.json());
        });
        this.http.get(AppSettings.API_ENDPOINT + '/empresas?status=0', header).subscribe( ret => {
            const retorno = ret.json();
            this.novasEmpresas = retorno.totalResultados;
            console.log(ret.json());
        });
        this.http.get(AppSettings.API_ENDPOINT + '/empresas?status=0', header).subscribe( ret => {
            const retorno = ret.json();
            this.novasEmpresas = retorno.totalResultados;
            console.log(ret.json());
        });

        this.alerts.push({
            id: 1,
            type: 'success',
            message: 'Em testes',
        }, {
            id: 2,
            type: 'warning',
            message: 'Qualquer erro, nos avisem pelo trello ou por email: dev@offcina.com!',
        });
    }
    ngOnInit() {}

    public closeAlert(alert: any) {
        const index: number = this.alerts.indexOf(alert);
        this.alerts.splice(index, 1);
    }
    empresa() {
        /* navega para detalher */
        this.router.navigate(['/empresas']);
    }
    associados() {
        /* navega para detalher */
        this.router.navigate(['/associados']);
    }
}
