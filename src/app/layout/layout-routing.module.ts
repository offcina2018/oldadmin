import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '', component: LayoutComponent,
        children: [
            { path: '', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'associados', loadChildren: '../associado/associado.module#AssociadoModule' },
            { path: 'empresas', loadChildren: '../empresa/empresa.module#EmpresaModule' },
            { path: 'notasfiscais', loadChildren: '../notafiscal/notafiscal.module#NotafiscalModule' },
            { path: 'areaatuacao', loadChildren: '../areaatuacao/areaatuacao.module#AreaatuacaoModule' },
            { path: 'enderecos', loadChildren: '../endereco/endereco.module#EnderecoModule' },
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'charts', loadChildren: './charts/charts.module#ChartsModule' },
            { path: 'tables', loadChildren: './tables/tables.module#TablesModule' },
            { path: 'forms', loadChildren: './form/form.module#FormModule' },
            { path: 'bs-element', loadChildren: './bs-element/bs-element.module#BsElementModule' },
            { path: 'grid', loadChildren: './grid/grid.module#GridModule' },
            { path: 'components', loadChildren: './bs-component/bs-component.module#BsComponentModule' },
            { path: 'blank-page', loadChildren: './blank-page/blank-page.module#BlankPageModule' },
            {path: 'perfil', loadChildren:'../associado/associado.module#AssociadoModule'},
            { path: 'pagamentos', loadChildren: '../pagamento/pagamento.module#PagamentoModule' },

            {path: 'infobancaria', loadChildren:'../infobancaria/infobancaria.module#InfobancariaModule'}



        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule { }
