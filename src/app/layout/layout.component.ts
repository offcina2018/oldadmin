import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {ApiLiame} from "../services/apiliame.service";

@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

    constructor(public router: Router,
    public apiliame: ApiLiame) { }

    ngOnInit() {
        if (this.router.url === '/') {
            this.router.navigate(['/dashboard']);
        }
    }

}
