import { BrowserModule } from '@angular/platform-browser';
import {HttpModule, Http} from '@angular/http';
import { Headers, JsonpModule, Jsonp } from '@angular/http';

import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import {DataTableModule} from 'angular2-datatable';
import {Pipe, PipeTransform} from "@angular/core";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuard } from './shared';
import { EmpresaModule } from './empresa/empresa.module';
import { NotafiscalModule} from './notafiscal/notafiscal.module';
import { PagamentoComponent } from './pagamento/pagamento.component';
import { ConteudoComponent } from './conteudo/conteudo.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { AssociadoModule} from './associado/associado.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { EnderecoModule} from './endereco/endereco.module';

import { statusAssociado, statusEmpresa, statusNotaFiscal, statusPagamento} from './app.settings';

import { ApiLiame} from './services/apiliame.service';
import { AuthService } from './auth.service';
import { WebStorageModule } from 'ngx-store';
import { EnderecoComponent } from './endereco/endereco.component';
import {NotafiscalService} from './service/notafiscal.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgNotifyPopup } from 'ng2-notify-popup';
import {EmpresaService} from './services/empresa.service';
import {InfobancariaModule} from './infobancaria/infobancaria.module';
import {InfobancariaService} from './services/infobancaria.service';
import {PagamentoModule} from './pagamento/pagamento.module';
import { StatusAssociadoPipe } from './pipes/status-associado.pipe';
import {EstaQuitadaPipe} from "./pipes/esta-quitada.pipe";
import { StatusEmpresaPipe } from './pipes/status-empresa.pipe';
import {NgxMaskModule} from "ngx-mask";
import {KzMaskDirective} from "./kz-mask.directive";
import { StatusNfPipe } from './pipes/status-nf.pipe';
import { LoadingModule } from 'ngx-loading';
import { StatusPagPipe } from './pipes/status-pag.pipe';
import { LOCALE_ID } from '@angular/core';
import {SimpleNotificationsModule} from "angular2-notifications/dist";

// import { DetalhesComponent } from './detalhes/detalhes.component';
// AoT requires an exported function for factories
export function HttpLoaderFactory(http: Http) {
    return new TranslateHttpLoader(http, '/assets/i18n/', '.json');
}

@NgModule({
    declarations: [
        AppComponent,
       // PagamentoComponent,
        ConteudoComponent,
        UsuarioComponent,
        StatusNfPipe,
        StatusPagPipe,

        // DetalhesComponent,
    ],
    imports: [
        WebStorageModule,
        JsonpModule,
        BrowserModule,
        FormsModule,
        HttpModule,
        LoadingModule,
        AssociadoModule,
        EmpresaModule,
        EnderecoModule,
        NotafiscalModule,
        AppRoutingModule,
        InfobancariaModule,
        PagamentoModule,
        NgxMaskModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [Http]
            }
        }),

        BrowserAnimationsModule,
        NgNotifyPopup,
        SimpleNotificationsModule.forRoot(),
        DataTableModule,
        NgxDatatableModule
    ],
    providers: [AuthGuard, ApiLiame, AuthService, EmpresaService, InfobancariaService,
    EstaQuitadaPipe, StatusEmpresaPipe, KzMaskDirective, StatusNfPipe,
        StatusAssociadoPipe, NotafiscalService, StatusPagPipe,
        {provide: LOCALE_ID, useValue: "pt-BR"}
        ],
    bootstrap: [AppComponent]
})
export class AppModule { }
