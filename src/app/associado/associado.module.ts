import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssociadoComponent } from './associado.component';
import { AssociadoRoutingModule } from './associado-routing.module';
import { PageHeaderModule } from '../shared';
import { DataTableModule } from 'angular2-datatable';
import { FormsModule } from '@angular/forms';
import {BrowserModule} from "@angular/platform-browser";
import {statusAssociado} from '../app.settings';
//  import {AssociadoDetalhesComponent} from './associado.component';
import {  ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { DataFilterPipe } from '../datafilter.pipe';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import {CpfPipe} from '../cpf.pipe' ;
import { TextMaskModule } from 'angular2-text-mask';
import {DetalhesComponent} from './detalhes.component';
import { AssociadoModel } from '../services/models/associado';
import {StatusAssociadoPipe} from "../pipes/status-associado.pipe";
import {NgxMaskModule} from 'ngx-mask';

@NgModule({
  imports: [
    CommonModule,
      // BrowserModule,
      AssociadoRoutingModule,
      PageHeaderModule,
      DataTableModule,
      FormsModule,
      HttpModule,
      NgxDatatableModule,
      NgxMaskModule,
      TextMaskModule,
      ReactiveFormsModule

  ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    ,
  declarations: [
      AssociadoComponent, DetalhesComponent, DataFilterPipe, CpfPipe, StatusAssociadoPipe
  ],
    exports: [
        AssociadoComponent, DetalhesComponent,
    ],
})
export class AssociadoModule { }
