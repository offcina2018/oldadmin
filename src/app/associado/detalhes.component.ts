import { RequestOptions, Headers, Http } from '@angular/http';
import {AppSettings, statusAssociado} from '../app.settings';
// import {Router} from "@angular/router";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ApiLiame } from '../services/apiliame.service';
import { AssociadoModel } from '../services/models/associado';
import {Page} from '../services/models/page';
import {ViewChild, OnInit, ViewEncapsulation, Component, Input} from '@angular/core';
import {CpfPipe} from '../cpf.pipe';
import {Form, FormBuilder, FormGroup, Validators, FormControl, FormArray} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';
import { NotificationService } from 'ng2-notify-popup';
import {NgxMaskModule} from 'ngx-mask';


/**
 * Created by offcina on 28/06/17.
 */
@Component({
    selector: 'app-detalhes',
    templateUrl: './detalhes.associado.component.html',
    styleUrls: ['./associado.component.scss'],
    providers: [ApiLiame,  CpfPipe, AssociadoModel, DatePipe , NotificationService ],
    encapsulation: ViewEncapsulation.None
})
export class DetalhesComponent implements OnInit {

    @ViewChild('myTable') table: any;
    @Input() ass: Observable<AssociadoModel>;
    loading = false;
    token: string;
    //   public cpf = [ ' ', /\d/, /\d/, /\d/, ' . ', /\d/, /\d/, /\d/, ' . ' , /\d/, /\d/, /\d/, ' - ', /\d/, /\d/];
    // public cpf = [ ' ', /\d/, /\d/, /\d/, ' . ', /\d/, /\d/, /\d/, ' . ' , /\d/, /\d/, /\d/,' - ', /\d/, /\d/]
    public specialCharacters: boolean;
    temp = [];
    rota: Router;
    public alerta: any = [];
    coordenadores: any;
    podeEditar: boolean;
    associado: AssociadoModel;
    url: string;
    handleError: any;
    // rows = [];
    idAssociado: any;

    errorMessage: any;
    page = new Page();
    rows: Array<AssociadoModel> = [];
    meuForm: any;
    fmBuilder: FormBuilder;
    statusAssoc: any;
    constructor(private route: ActivatedRoute,
                private router: Router,
                public api: ApiLiame ,
                public api2: Http,
                private fb: FormBuilder,
                private http: Http,
                public notificationService: NotificationService
                ) {
        this.podeEditar = false;
        this.meuForm = new FormGroup({
        nome: new FormControl ({value: '', disabled: true}, Validators.required),
        nome_artistico: new FormControl ({value: '', disabled: true}, Validators.required),
        email: new FormControl ({value: '', disabled: true}, Validators.required),
        identidade: new FormControl ({value: '', disabled: true}, Validators.required),
        data_nascimento: new FormControl ({value: '', disabled: true}, Validators.required),
        cpf : new FormControl ({value: '', disabled: true}, Validators.required),
        coordenador : new FormControl ({value: '', disabled: true}, Validators.required),
        orgao_expedidor: new FormControl ({value: '', disabled: true}, Validators.required),
        tipo : new FormControl ({value: '1', disabled: true}, Validators.required),
        taxa_adm_porcentagem : new FormControl ({value: '0', disabled: true}, Validators.required),
        cpf_responsavel : new FormControl ({value: '', disabled: true}, Validators.required),
        identidade_responsavel: new FormControl ({value: '', disabled: true}, Validators.required),
        // telefone_residencial: new FormControl ({value: '', disabled: true}, Validators.required),
        orgao_expedidor_responsavel: new FormControl ({value: '', disabled: true}, Validators.required),
        assinatura: new FormControl(''),
        createdAt: new FormControl (''),
        updatedAt: new FormControl(''),
        user: new FormControl(''),
        nome_responsavel: new FormControl ({value: '', disabled: true}, Validators.required),
        id: new FormControl ({value: '', disabled: true}, Validators.required),
        is_coordenador: new FormControl ({value: '0', disabled: true}, Validators.required),
        status: new FormControl ({value: '0', disabled: true}, Validators.required),
        // auditor : new FormControl('',Validators.required),


        });
        this.route.params
            // .switchMap((params: Params) => this.api.getAssociado(params['id']))
            /*.subscribe((ret: any) => {
                (<FormGroup>this.meuForm).setValue(ret, {onlySelf: true});
                this.idAssociado = ret.id;  telefone_residencial

            });*/
            .subscribe(params => {
                this.idAssociado =  params['id'];
                console.log(' sou do time ' + this.idAssociado);
                if (this.idAssociado > 0) {
                    this.api.getAssociado(this.idAssociado)
                        .subscribe((ret: any) => {
                            this.associado = ret;
                            (<FormGroup>this.meuForm).setValue(ret, {onlySelf: true});
                            this.meuForm.removeControl('assinatura');
                            this.meuForm.removeControl('user');
                            this.meuForm.removeControl('createdAt');
                            this.meuForm.removeControl('updatedAt');
                         //   this.meuForm.controls['telefone_residencial'].setValue(moment(ret.telefone_residencial));
                            this.meuForm.controls['data_nascimento'].setValue(moment(ret.data_nascimento).format('YYYY-MM-DD'));
                            this.idAssociado = ret.id;

                        }, erro => {
                            this.notificationService.show(erro.toString(), {
                                position: 'bottom',
                                duration: '2000',
                                type: 'error' });
                        });
                }
            });
        this.api.getCoordenadores(this.page)
            .subscribe(ret => {
            this.coordenadores = ret ;
        }, erro => {
                this.notificationService.show(erro.toString(), {
                    position: 'bottom',
                    duration: '2000',
                    type: 'error' });
            });
        console.log('status');
        const s = Object.keys(statusAssociado);
        this.statusAssoc = s.slice(s.length / 2);
    }

    ngOnInit() {


    };
    editar() {
        const ctrl = this.meuForm.get('nome');
        ctrl.enabled ? ctrl.disable() : ctrl.enable();
        const ctrl1 = this.meuForm.get('nome_artistico');
        ctrl1.enabled ? ctrl1.disable() : ctrl1.enable();
        const ctrl2 = this.meuForm.get('email');
        ctrl2.enabled ? ctrl2.disable() : ctrl2.enable();
        const ctrl3 = this.meuForm.get('identidade');
        ctrl3.enabled ? ctrl3.disable() : ctrl3.enable();
        const ctrl4 = this.meuForm.get('data_nascimento');
        ctrl4.enabled ? ctrl4.disable() : ctrl4.enable();
        const ctrl5 = this.meuForm.get('cpf');
        ctrl5.enabled ? ctrl5.disable() : ctrl5.enable();
        const ctrl6 = this.meuForm.get('coordenador');
        ctrl6.enabled ? ctrl6.disable() : ctrl6.enable();
        const ctrl7 = this.meuForm.get('orgao_expedidor');
        ctrl7.enabled ? ctrl7.disable() : ctrl7.enable();
        const ctrl8 = this.meuForm.get('tipo');
        ctrl8.enabled ? ctrl8.disable() : ctrl8.enable();
        const ctrl9 = this.meuForm.get('taxa_adm_porcentagem');
        ctrl9.enabled ? ctrl9.disable() : ctrl9.enable();
        const ctrl10 = this.meuForm.get('cpf_responsavel');
        ctrl10.enabled ? ctrl10.disable() : ctrl10.enable();
        const ctrl11 = this.meuForm.get('identidade_responsavel');
        ctrl11.enabled ? ctrl11.disable() : ctrl11.enable();
        const ctrl12 = this.meuForm.get('orgao_expedidor_responsavel');
        ctrl12.enabled ? ctrl12.disable() : ctrl12.enable();
        const ctrl13 = this.meuForm.get('nome_responsavel');
        ctrl13.enabled ? ctrl13.disable() : ctrl13.enable();
        const ctrl14 = this.meuForm.get('is_coordenador');
        ctrl14.enabled ? ctrl14.disable() : ctrl14.enable();
        const ctrl15 = this.meuForm.get('status');
        ctrl15.enabled ? ctrl15.disable() : ctrl15.enable();
    }
    cadastraNF(idass, idbanco) {
        this.router.navigate(['/notasfiscais/associados/' + idass + '/dados', idbanco]);
    }
    voltaAss() {
        this.router.navigate(['/associados']);
    }
    mudaStatusAssociado() {

        // cria header
        // chama put so c status
        // mostra barra de salvo
        const opt = this.api.montaHeader();
        console.log(this.api.url + '/associados/' + this.idAssociado,
            'status=' + this.meuForm.controls['status'].getValue());
        this.http.put(this.api.url + '/associados/' + this.idAssociado,
            'status=' + this.meuForm.controls['status'].getValue(),
            opt).subscribe(ret => {
            this.notificationService.show('Salvo', {
                position: 'bottom',
                duration: '2000',
                type: 'success' });
        }, erro => {
            this.notificationService.show('ERRO!', {
                position: 'bottom',
                duration: '2000',
                type: 'error' });
        });
    }
    salvarAssociado() {
        console.log('salva');
        this.token = AppSettings.apitoken;
        this.url = AppSettings.API_ENDPOINT;
        this.alerta.avisoSucesso = '';
        this.meuForm.controls['status'].setValue(1);
        /*if (this.meuForm.controls['coordenador'].value == "0") {
            this.meuForm.removeControl('coordenador');
        }*/
        this.meuForm.removeControl('user');
        this.meuForm.removeControl('assinatura');

//        console.log(this.api.saveEmpresa(this.idEmpresa));
        this.api.saveAssociado(this.idAssociado, this.meuForm.value).subscribe(x => {
            console.log(this.idAssociado);
            this.notificationService.show('Salvo', {
                position: 'bottom',
                duration: '2000',
                type: 'success' });
        }, erro => {
            this.notificationService.show('ERRO!', {
                position: 'bottom',
                duration: '2000',
                type: 'error' });
        });

        //  this.router.navigate(['/empresas']);

    }

    deletaAssociado(id) {
        //    console.log(idEmpresa);
        //  const id = linha.idEmpresa;
        console.log('deleta associ');
        this.token = AppSettings.apitoken;
        this.url = AppSettings.API_ENDPOINT;
        this.alerta.avisoSucesso = '';

        this.api.delAssociado(id).do(function (x) {
            this.notificationService.show('Removido', {
                position: 'bottom',
                duration: '2000',
                type: 'success' });

        })
            .catch(this.handleError)
            .subscribe((ok) => {
                this.notificationService.show('Erro', {
                    position: 'bottom',
                    duration: '2000',
                    type: 'error' });
            });
        // this.router.navigate(['/associados']);


    }

    loga(event) {
        console.log(event);
    }
}

