/**
 * Created by paulomarinho on 28/05/17.
 */


import {NgModule} from  '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { AssociadoComponent } from './associado.component';
import { DetalhesComponent } from './detalhes.component';
import { AuthGuard } from '../shared';


const routes: Routes = [

    {path: '', component: AssociadoComponent,
        canActivate: [AuthGuard]},
    {path: 'perfil/:id', component: DetalhesComponent,
        canActivate: [AuthGuard]}

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AssociadoRoutingModule {
}
