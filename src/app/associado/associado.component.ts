import { Component, OnInit } from '@angular/core';
import { ApiLiame } from '../services/apiliame.service';
import { Observable } from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';
import {Http, RequestOptions} from '@angular/http';
import {Headers} from '@angular/http';
import {Page} from '../services/models/page';
import { AssociadoModel } from '../services/models/associado';
import {  ViewEncapsulation, ViewChild } from '@angular/core';
import {PagedData} from '../services/models/paged-data' ;
import {AppSettings} from '../app.settings';
import { CpfPipe} from '../cpf.pipe';
import {DetalhesComponent} from './detalhes.component';
import { AuthService } from '../auth.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {id} from "@swimlane/ngx-datatable/release/utils";
import { NotificationService } from 'ng2-notify-popup';
import { StatusAssociadoPipe} from '../pipes/status-associado.pipe';
import { TextMaskModule } from 'angular2-text-mask';
import {NotificationsService} from "angular2-notifications/dist";

@Component({
  selector: 'app-associado',
  templateUrl: './associado.component.html',
  styleUrls: ['./associado.component.scss'],
    providers: [ApiLiame,  CpfPipe , NotificationService],
    encapsulation: ViewEncapsulation.None
})
export class AssociadoComponent implements OnInit {

    /*setando variavies */

        @ViewChild('myTable') table: any;
        loading = false;
        lstAssociados: any;
        token: string;
        busca: string;
        public celular = ['(', /[1-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
        public cpf = [ ' ', /\d/, /\d/, /\d/, ' . ', /\d/, /\d/, /\d/, ' . ' , /\d/, /\d/, /\d/, ' - ', /\d/, /\d/]

    temp = [];
        rota: Router;
        public alerta: any = [];
        url: string;
        handleError: any;
        associados: Array<AssociadoModel> = [];
        errorMessage: any;
        page = new Page();
        autenticador: AuthService;
        rows: Array<AssociadoModel> = [];
        paginacao: any = {
        limit: 0,
            totalResultados: 0,
            paginaAtual: 1
    };
    constructor(
                public api: ApiLiame,
                public api2: Http,
                public auth: AuthService,
                public router: Router ,
                public http: Http,
                private activatedRoute: ActivatedRoute,
    public notificationService: NotificationService,
                private _service: NotificationsService) {
        /* seta variaveis iniciais de pagina */
        this.page.pageNumber = 1;
        this.page.camposBusca = ['nome', 'nome_artistico'];
        this.page.auxiliares = ['info_bancaria', 'endereco', 'nfs', 'area_atuacao', 'pagamentos'];
        this.rota = router;
        this.autenticador = auth;
        this.page.size = 20;
        this.comboAssociados();
    };

    ngOnInit() {
        if (!this.autenticador.checkLogin()) {
             this.rota.navigate(['/dashboard']);
        }
        this.activatedRoute.params.subscribe((params: Params) => {
            let caso = params['caso'];
            let msg = params['msg'];
            if (caso && msg) {
                this.notificationService.show(msg, {
                    position:'bottom',
                    duration:'2000',
                    type: caso });
            }else{
                console.log('sem parametros');
            }
        });
        this.setPage({ offset: 0 });
    };

    /**
     * Navega para detalhes
     * @param id id do associado
     */
    detalhes(id) {
        /* navega para detalhes */
        this.router.navigate(['/associados/perfil', id]);
    }

    /**
     * Novo cadastro
     */
    irParaCadastro() {
        /* navega para detalher */
        this.router.navigate(['/associados/perfil', 0]);
    }
    cadastraInfoBancaria(idass, idbanco) {
        this.router.navigate(['/infobancaria/associados/' + idass + '/dados', idbanco]);
    }
    cadastraNF(idass, idbanco) {
        this.router.navigate(['/notasfiscais/associados/' + idass + '/dados', idbanco]);
    }
    /* cadastrar endereco para usuario */
    cadastrarEnderecoUsuario(iduser, id) {
         this.router.navigate(['/enderecos/associados/' + iduser + '/endereco', id ]);
    }
    /* cadastrar area de atuacao */
    cadastrarAreaAtuacao(iduser) {
        this.router.navigate(['/areeaatuacao/associados/' + iduser ]);
    }
    /**
     * Filtrar por campo de busca
     */
    filtrar() {

       this.page.valorBusca =  this.busca;
       this.api.getAssociados(this.page).subscribe(
            pagedData =>  this.rows = pagedData,
           erro => {
               this.notificationService.show(erro.toString(), {
                   position: 'bottom',
                   duration: '2000',
                   type: 'error' });
           }
        );
    }

    /**
     * ORDENACAO
     * @param event evento
     */
    onSort(event) {

        this.page.tipoOrdenacao = event.newValue;
        this.page.campoOrdenacao = event.sorts[0].prop;
        this.page.pageNumber = 1;
        this.loading = true;
        this.api.getAssociados(this.page).subscribe(
            pagedData =>  {
                this.rows = pagedData.data;
                this.paginacao = pagedData;
            },
            erro => {
                this.notificationService.show(erro.toString(), {
                    position: 'bottom',
                    duration: '2000',
                    type: 'error' });
            }
        );
        this.associados = this.rows;

        this.loading = false;
    }



    /**
     * Paginacao
     * @param page Pagina
     */
    setPage(pageInfo) {
        // console.log(pageInfo);
        this.page.pageNumber = pageInfo.offset;
        this.api.getAssociados(this.page).subscribe(
             pagedData =>  {
                 this.rows = pagedData.data;
                 this.paginacao = pagedData;
             },
            erro => {
                this.notificationService.show(erro.toString(), {
                    position: 'bottom',
                    duration: '2000',
                    type: 'error' });
            }
        );
         this.associados = this.rows;
    }
    changeLstAssociados(x) {
        if (x.value == 0) {
            delete this.page.fixo.associado;
        }else {
            this.page.fixo.associado = x.value;
        }

        this.setPage({ offset: 0 });
        console.log(x.value);
    }
    changeLstEmail(x) {
        if (x.value == 0) {
            delete this.page.fixo.email;
        }else {
            this.page.fixo.email = x.value;
        }

        this.setPage({ offset: 0 });
        console.log(x.value);
    }
    comboAssociados() {
        const page = new Page();
        page.pageSize = 5000;
        page.pageNumber = 0;
        page.camposBusca = ['numero', 'nome', 'email'];
        page.camposRetorno = ['id', 'nome', 'email'];
        page.campoOrdenacao = 'email';
        page.tipoOrdenacao = 'asc';
        this.api.getAssociados(page).subscribe(ret => {
            this.lstAssociados = ret;
            console.log('leu');
        });
    }
    /**
     * Abre Linha
     * @param row numero da linha
     */
    toggleExpandRow(row) {
        // console.log('Toggled Expand Row!', row);
        this.table.rowDetail.toggleExpandRow(row);
    }

    onDetailToggle(event) {
        // console.log('Detail Toggled', event);
    }
    aprovar(id) {
        this.alteraStatus(id, 4, ' Aprovado ');

    }
    negar(id) {
        this.alteraStatus(id, 3, ' Negado ');
    }
    desligar(id) {
        this.alteraStatus(id, 2, ' Desligado ');
    }
    editar(id) {
        this.router.navigate(['/associados/perfil', id]);
    }
    alteraStatus(idAssociado, status, frase) {
        console.log(' alteraStatus ');
        const opt = this.api.montaHeader();
        console.log(this.api.url + '/associados/' + idAssociado,
            'status=' + status);
        this.http.put(this.api.url + '/associados/' + idAssociado,
            'status=' + status, opt).subscribe(ret => {
            this._service.success(
                frase,
                frase,
                {
                    timeOut: 4000,
                    showProgressBar: true,
                    pauseOnHover: false,
                    clickToClose: false,
                    maxLength: 10
                }
            );
        }, erro => {

        });
    }


}
