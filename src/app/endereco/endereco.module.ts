import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import { EnderecoComponent } from './endereco.component';
import { EnderecoRoutingModule } from './endereco-routing.module';
import { PageHeaderModule } from '../shared';
import { DataTableModule } from 'angular2-datatable';
import { FormsModule } from '@angular/forms';
import {BrowserModule} from "@angular/platform-browser";
//  import {AssociadoDetalhesComponent} from './associado.component';
import {  ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import {CpfPipe} from '../cpf.pipe' ;
import { TextMaskModule } from 'angular2-text-mask';
import {DetalhesEnderecoComponent} from './detalhes.endereco.component';
import { AssociadoModel } from '../services/models/associado';

@NgModule({
  imports: [
    CommonModule,
      // BrowserModule,
      EnderecoRoutingModule,
      PageHeaderModule,
      DataTableModule,
      FormsModule,
      HttpModule,
      NgxDatatableModule,
      TextMaskModule,
      ReactiveFormsModule
  ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    ,
  declarations: [
      EnderecoComponent, DetalhesEnderecoComponent
  ],
    exports: [
        EnderecoComponent, DetalhesEnderecoComponent
    ],
})
export class EnderecoModule { }
