import { Component, OnInit } from '@angular/core';
import { RequestOptions, Headers, Http } from '@angular/http';
import { AppSettings } from '../app.settings';
// import {Router} from "@angular/router";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ApiLiame } from '../services/apiliame.service';
import {Page} from '../services/models/page';
import {ViewChild,  ViewEncapsulation,  Input} from '@angular/core';
import {Form, FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';
import { AuthService } from '../auth.service';
import { EnderecoModel} from '../services/models/endereco';

@Component({
  selector: 'app-endereco',
  templateUrl: './endereco.component.html',
  providers: [ApiLiame, DatePipe ],
  styleUrls: ['./endereco.component.scss']

})
export class EnderecoComponent implements OnInit {
	 @ViewChild('myTable') table: any;
    loading = false;
    token: string;
    public celular = ['(', /[1-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
    temp = [];
    rota: Router;
    public alerta: any = [];
    url: string;
    handleError: any;
    associados: Array<EnderecoModel> = [];
    errorMessage: any;
    page = new Page();
    autenticador: AuthService;
    rows: Array<EnderecoModel> = [];

    constructor(public api: ApiLiame,
                public api2: Http,
                public auth: AuthService,
                public router: Router ) {
        /* seta variaveis iniciais */
        this.page.pageNumber = 0;
        this.rota = router;
        this.autenticador = auth;
        this.page.size = 20;
    };
    ngOnInit() {
        if (!this.autenticador.checkLogin()) {
             this.rota.navigate(['/dashboard']);
        }
        this.setPage({ offset: 0 });
    };

    /**
     * Navega para detalhes
     * @param id id do associado
     */
    detalhes(id) {
        /* navega para detalher */
        this.router.navigate(['/enderecos/dados',id]);
    }

    /**
     * Novo cadastro
     */
    irParaCadastro() {
        /* navega para detalher */
        this.router.navigate(['/endereco/dados',0]);
    }
    /**
     * PM - NAO VALE
     * @param event
     */
    updateFilter(event) {
        const val = event.target.value.toLowerCase();
        const temp = this.rows.filter(function(d) {
            return d.rua.toLowerCase().indexOf(val) !== -1 || !val;
        });
        this.rows = temp;
        this.table.setPage = 0;
    }

    /**
     * PM - NAO VALE
     */
    atualizaFiltro() {
        this.page.valorBusca = 'Ron';
        /*this.serverResultsService.getResults(this.page).subscribe(
            pagedData => setTimeout(() => this.rows = pagedData, 10)
        );*/
    }

    /**
     * ORDENACAO
     * @param event evento
     */
    onSort(event) {

        this.page.tipoOrdenacao = event.newValue;
        this.page.campoOrdenacao = event.sorts[0].prop ;
        console.log(this.page);
        this.page.pageNumber = 0;
        this.loading = true;
        this.api.listaEnderecos(this.page).subscribe(
            // pagedData =>  this.rows = pagedData,
            pagedData =>  this.rows = pagedData,
        );
        this.associados = this.rows;

        this.loading = false;
    }



    /**
     * Paginacao
     * @param page Pagina
     */
    setPage(pageInfo) {
        console.log(pageInfo);
        this.page.pageNumber = pageInfo.offset;
        this.api.listaEnderecos(this.page).subscribe(
             pagedData =>  this.rows = pagedData,
            // pagedData =>  this.associados = pagedData,
        );
         this.associados = this.rows;
    }

    /**
     * Abre Linha
     * @param row numero da linha
     */
    toggleExpandRow(row) {
        console.log('Toggled Expand Row!', row);
        this.table.rowDetail.toggleExpandRow(row);
    }

    onDetailToggle(event) {
        console.log('Detail Toggled', event);
    }
}
