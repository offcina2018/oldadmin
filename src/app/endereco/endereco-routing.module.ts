import {NgModule} from  '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { AuthGuard } from '../shared';
import { EnderecoComponent} from "./endereco.component";
import {DetalhesEnderecoComponent} from "./detalhes.endereco.component";


const routes: Routes = [

    {path: '', component: EnderecoComponent,
        canActivate: [AuthGuard]},
    {path: 'dados/:id', component: DetalhesEnderecoComponent,
        canActivate: [AuthGuard]},
    {path: ':tipoentidade/:identidade/endereco/:id', component: DetalhesEnderecoComponent,
        canActivate: [AuthGuard]},
    

];

@NgModule({

    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]

})
export class EnderecoRoutingModule {
}
