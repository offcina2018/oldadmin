import { RequestOptions, Headers, Http } from '@angular/http';
import { AppSettings } from '../app.settings';
// import {Router} from "@angular/router";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ApiLiame } from '../services/apiliame.service';
import { EnderecoModel } from '../services/models/endereco';
import {Page} from '../services/models/page';
import {ViewChild, OnInit, ViewEncapsulation, Component, Input} from '@angular/core';
import {Form, FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';
import {NotificationService} from 'ng2-notify-popup';

/**
 * Created by offcina on 28/06/17.
 */
@Component({
    selector: 'app-detalhes',
    templateUrl: './detalhes.endereco.component.html',
    styleUrls: ['./detalhes.endereco.component.scss'],
    providers: [ApiLiame, EnderecoModel, DatePipe, NotificationService ],
    encapsulation: ViewEncapsulation.None
})
export class DetalhesEnderecoComponent implements OnInit {

    /* propriedades */
    @ViewChild('myTable') table: any;
    @Input() ass: Observable<EnderecoModel>;
    loading = false;
    token: string;
    public celular = ['(', /[1-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
    temp = [];
    rota: Router;
    public alerta: any = [];
    coordenadores: any;
    url: string;
    handleError: any;
    // rows = [];
    idEndereco: any;
    idEntidade: any;
    tipoEntidade: any;
    errorMessage: any;
    page = new Page();
    rows: Array<EnderecoModel> = [];
    meuForm: any;
    fmBuilder: FormBuilder;
    constructor(private route: ActivatedRoute,
                private router: Router,
                public api: ApiLiame ,
                public api2: Http,
                private fb: FormBuilder,
                private associado: EnderecoModel,
    public notificationService: NotificationService) {

        /* setup form */
        this.meuForm = new FormGroup({
                    cep: new FormControl ('00000', Validators.required),
                    rua: new FormControl ('', Validators.required),
                    numero: new FormControl ('', Validators.required),
                    complemento: new FormControl ('', Validators.required),
                    pais: new FormControl ('', Validators.required),
                    estado : new FormControl('', Validators.required),
                    cidade : new FormControl('', Validators.required),
                    bairro: new FormControl ('', Validators.required),
                    is_ativo : new FormControl('1', Validators.required),
                    obs : new FormControl('', Validators.required),
                    id : new FormControl('', Validators.required),
                    createdAt : new FormControl('', Validators.required),
                    updatedAt : new FormControl('', Validators.required),
                    // auditor : new FormControl('',Validators.required),
                    });
                    this.route.params
                        .subscribe(params => {
                            this.idEndereco =  params['id'];
                            this.idEntidade =  params['identidade'];
                            this.tipoEntidade =  params['tipoentidade'];

                            console.log(' sou do time ' + this.idEndereco);

                            if (this.idEndereco > 0 ) {
                                this.api.getEndereco(this.idEndereco)
                                    .subscribe((ret: any) => {

                                        (<FormGroup>this.meuForm).setValue(ret, {onlySelf: true});
                                        this.idEndereco = ret.id;

                                    });
                            }
                        });
    }

    ngOnInit() {


    };
    salvaEndereco() {
        /* salva endereco pela entidade relacionada */
        this.alerta.avisoSucesso = '';
        this.api.salvaEndereco(this.idEndereco, this.meuForm.value, this.idEntidade, this.tipoEntidade).subscribe(x => {
            this.notificationService.show('Salvo', {
                position: 'bottom',
                duration: '2000',
                type: 'success' });
            // this.router.navigate(['/empresas']);
        });
    }
    deletaEndereco(id) {
        /* deleta endereco pela entidade relacionada */
        console.log('deleta ende');
        this.alerta.avisoSucesso = '';

        this.api.deletaEndereco(id).do(x => {
            this.notificationService.show('Salvo', {
                position: 'bottom',
                duration: '2000',
                type: 'success' });
            // this.setPage(1);
            // this.router.navigate(['/empresas']);

        },
        erro => {
            this.notificationService.show('Erro', {
                position: 'bottom',
                duration: '2000',
                type: 'success' });
        })
            .catch(this.handleError)
            .subscribe((ok) => {
                console.log(ok);
            });
        this.router.navigate(['/associados']);


    }
    voltaTipo() {
        this.router.navigate(['/' + this.tipoEntidade]);
    }
    loga(event) {
        console.log(event);
    }
}

