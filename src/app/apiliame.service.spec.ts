import { TestBed, inject } from '@angular/core/testing';

import { ApiliameService } from './apiliame.service';

describe('ApiliameService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApiliameService]
    });
  });

  it('should be created', inject([ApiliameService], (service: ApiliameService) => {
    expect(service).toBeTruthy();
  }));
});
