import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AuthService} from '../auth.service';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {PageHeaderModule} from "../shared/modules/page-header/page-header.module";
import {ApiLiame} from "../services/apiliame.service";

@NgModule({
  imports: [
    CommonModule,
    LoginRoutingModule,
      FormsModule,
      PageHeaderModule,
  ],
  declarations: [LoginComponent],
    providers: [AuthService, ApiLiame]
})
export class LoginModule { }
