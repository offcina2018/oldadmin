import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import {Routes, RouterModule, Router} from '@angular/router';
import {  LocalStorageService } from 'ngx-store';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    auth: AuthService;
    user = [];
    retorno: any;
    rota: Router;
    constructor(public a: AuthService, public router: Router) {
        this.auth = a;
        this.rota = router;
    }
    ngOnInit() { }

    onLoggedin() {

        console.log('batata show');
        localStorage.setItem('isLoggedin', 'true');
    }
    telaCadastro() {
        console.log('batata show2');
    }
    logar() {
        this.auth.login(this.user).then(data => {
            this.retorno = data;
            console.log(this.retorno);
            if (this.retorno) {
                console.log('indo pra dashboard');
                this.router.navigate(['/dashboard']);
            }
        });
    }
    cadastrar() {
        this.auth.register(this.user).then(data => {
            this.retorno = data;
            if (this.retorno) {
                // PM ta dando erro nesse redirect
                this.router.navigate(['/dashboard']);
            }
            console.log('retorno');
            console.log(this.retorno);
        });
    }

}
