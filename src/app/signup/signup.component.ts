import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import {Routes, RouterModule, Router} from '@angular/router';
import {  LocalStorageService } from 'ngx-store';


@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
    auth: AuthService;
    user = [];
    retorno: any;

    constructor(localStorageService: LocalStorageService, public router: Router, p: AuthService) {
        this.auth = p;
    }

    ngOnInit() { }
    cadastrar() {
        this.auth.register(this.user).then(data => {
            this.retorno = data;
            if (this.retorno) {
                this.router.navigate(['/dashboard']);
            }
            console.log('retorno');
            console.log(this.retorno);
        });
    }
}
