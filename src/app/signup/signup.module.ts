import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthService} from '../auth.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SignupRoutingModule } from './signup-routing.module';
import { SignupComponent } from './signup.component';

@NgModule({
  imports: [
    CommonModule,
    SignupRoutingModule, FormsModule
  ],
  declarations: [SignupComponent],
    providers: [AuthService]

})
export class SignupModule { }
