import {NgModule} from  '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { AuthGuard } from '../shared';
import { DetalhesInfobancariaComponent} from './detalhes.infobancaria.component'
import { InfobancariaComponent} from './infobancaria.component';

const routes: Routes = [

    {path: '', component: InfobancariaComponent,
        canActivate: [AuthGuard]},
    {path: 'dados/:id', component: DetalhesInfobancariaComponent,
        canActivate: [AuthGuard]},
    {path: ':tipoentidade/:identidade/dados/:id', component: DetalhesInfobancariaComponent,
        canActivate: [AuthGuard]},


];

@NgModule({

    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]

})
export class InfobancariaRoutingModule {
}
