import { RequestOptions, Headers, Http } from '@angular/http';
import { AppSettings } from '../app.settings';
// import {Router} from "@angular/router";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ApiLiame } from '../services/apiliame.service';
import { EnderecoModel } from '../services/models/endereco';
import {Page} from '../services/models/page';
import {ViewChild, OnInit, ViewEncapsulation, Component, Input} from '@angular/core';
import {Form, FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';
import {AreaatuacaoModel} from '../services/models/areaatuacao';
import { NotificationService } from 'ng2-notify-popup';
import {InfobancariaService} from "../services/infobancaria.service";

/**
 * Created by offcina on 28/06/17.
 */
@Component({
    selector: 'app-detalhes',
    templateUrl: './detalhes.infobancaria.component.html',
    styleUrls: ['./detalhes.infobancaria.component.scss'],
    providers: [ApiLiame, EnderecoModel, DatePipe , InfobancariaService,NotificationService],
    encapsulation: ViewEncapsulation.None
})
export class DetalhesInfobancariaComponent implements OnInit {

    /* propriedades */
    @ViewChild('myTable') table: any;
    @Input() ass: Observable<EnderecoModel>;
    loading = false;
    token: string;
    public celular = ['(', /[1-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
    temp = [];
    rota: Router;
    public alerta: any = [];
    coordenadores :any;
    url: string;
    handleError: any;
    // rows = [];
    idInfo: any;
    idEntidade: any;
    tipoEntidade: any;
    errorMessage: any;
    page = new Page();
    rows: Array<EnderecoModel> = [];
    meuForm: any;
    fmBuilder: FormBuilder;
    area: AreaatuacaoModel;
    areas: Array<AreaatuacaoModel>;
    constructor(private route: ActivatedRoute,
                private router: Router,
                public api: ApiLiame ,
                public api2: Http,
                public apiinfobancaria: InfobancariaService,
                private fb: FormBuilder,
                private associado: EnderecoModel,
                public notificationService: NotificationService) {

        /* setup form */
        this.meuForm = new FormGroup({
                    cod_banco: new FormControl ('', Validators.required),
                    titular: new FormControl ('', Validators.required),
                    agencia: new FormControl ('', Validators.required),
                    conta: new FormControl ('', Validators.required),
                    tipo_conta: new FormControl ('', Validators.required),
                    is_conjunta: new FormControl ('', Validators.required),
                    //is_ativo: new FormControl ('', Validators.required),
                    tempid: new FormControl ('', Validators.required),
                    obs: new FormControl ('', Validators.required),
                    //eSubItem: new FormControl(''),
                    id : new FormControl('',Validators.required),
                    createdAt : new FormControl('',Validators.required),
                    updatedAt : new FormControl('',Validators.required),
                    auditor : new FormControl('',Validators.required),
                    });
                    this.route.params
                        .subscribe(params => {
                            this.idInfo =  params['id'];
                            this.idEntidade =  params['identidade'];
                            this.tipoEntidade =  params['tipoentidade'];

                            console.log(' sou do time ' + this.idInfo);

                            if (this.idInfo > 0 ) {
                                this.apiinfobancaria.getInfoBancaria(this.idInfo)
                                    .subscribe((ret: any) => {
                                        this.area = ret;
                                        (<FormGroup>this.meuForm).setValue(ret, {onlySelf: true});
                                        this.idInfo = ret.id;

                                    });
                            }
                        });

    }

    ngOnInit() {


    };
    salvaInfoBancaria() {
        /* salva endereco pela entidade relacionada */
        this.alerta.avisoSucesso = '';
       // this.meuForm.controls['is_sub_item'].setValue();
        this.meuForm.removeControl('tempid');

        this.apiinfobancaria.saveInfobancaria(this.idInfo,
            this.meuForm.value,
            this.idEntidade, this.tipoEntidade).subscribe(x => {
            this.notificationService.show('Salvo', {
                position: 'bottom',
                duration: '2000',
                type: 'error' });
            this.idInfo = x.id;
            //this.router.navigate(['/areaatuacao']);
        },
        erro => {
            this.notificationService.show(erro.toString(), {
                position: 'bottom',
                duration: '2000',
                type: 'error' });
        });
    }
    voltaAss(){
        this.router.navigate(['/associados']);
    }
    deletaInfoBancaria(id) {
        /* deleta endereco pela entidade relacionada */
        console.log('deleta ende');
        this.alerta.avisoSucesso = '';

        this.apiinfobancaria.deletaInfobancaria(id).do(x => {
            this.notificationService.show(' Deletado', {
                position: 'bottom',
                duration: '2000',
                type: 'error' });

        },erro => {
            this.notificationService.show(erro.toString(), {
                position: 'bottom',
                duration: '2000',
                type: 'error' });
        })
            .catch(this.handleError)
            .subscribe((ok) => {
                console.log(ok);
            });
        //this.router.navigate(['/associados']);


    }

    loga(event){
        console.log(event);
    }
}

